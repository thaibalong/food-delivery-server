define({ "api": [
  {
    "version": "2.0.1",
    "type": "get",
    "url": "/district/getAll",
    "title": "get all district",
    "group": "Address",
    "description": "<p>Get all district in HCM city</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      }
    },
    "filename": "app/router/address.js",
    "groupTitle": "Address",
    "name": "GetDistrictGetall"
  },
  {
    "version": "2.0.1",
    "type": "get",
    "url": "/ward/getAllByDistrict",
    "title": "get ward by district",
    "group": "Address",
    "description": "<p>Get all wards by district</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "id",
            "description": "<p>The id of district</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      }
    },
    "filename": "app/router/address.js",
    "groupTitle": "Address",
    "name": "GetWardGetallbydistrict"
  },
  {
    "version": "2.0.0",
    "type": "get",
    "url": "/categories/getAll",
    "title": "Get all categories",
    "group": "Categories",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID of category</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of category</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "[\n {\n   \"id\": 1,\n   \"name\": \"Cơm trưa\",\n    \"image\": \"http://cdn.congso.net/asset/home/img/500/578de030ad67a_1468915760.jpg\"\n },\n {\n   \"id\": 2,\n   \"name\": \"Đồ uống\"\n \"image\": \"https://hd1.hotdeal.vn/images/94221_body_2.jpg\"\n },\n {\n   \"id\": 3,\n   \"name\": \"Đồ ăn\"\nimage\": \"https://media.gody.vn//images/hinh-tong-hop/am-thuc-hue-gody/1-2018/32674114-20180103094505-hinh-tong-hop-am-thuc-hue-gody.jpg\"\n },\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          }
        ]
      }
    },
    "filename": "app/router/restaurant.js",
    "groupTitle": "Categories",
    "name": "GetCategoriesGetall"
  },
  {
    "version": "2.0.1",
    "type": "get",
    "url": "/food/:id",
    "title": "Get food by id",
    "group": "Food",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\"food\": {\n   \"id\": 1301,\n   \"name\": \"Combo \\\"Khai trương tưng bừng\\\" giá 449k (2 - 3 người)\",\n   \"idFoodCategory\": 6,\n   \"idRestaurant\": 13,\n   \"image\": \"https://media.foody.vn/res/g12/113651/s600x600/201891482210-bua-ngon-du-day.jpg\",\n   \"price\": 289000,\n   \"sold\": 56\n}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/router/food.js",
    "groupTitle": "Food",
    "name": "GetFoodId"
  },
  {
    "version": "2.0.1",
    "type": "get",
    "url": "/food/searchfood",
    "title": "Search food by name",
    "group": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "foodname",
            "description": "<p>name of food</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "[\n\n   {\n       \"id\": 1004,\n       \"name\": \"Trà dâu Nam Mỹ chanh vàng\",\n       \"idFoodCategory\": 2,\n       \"idRestaurant\": 22,\n       \"price\": 50000,\n       \"sold\": 263,\n       \"image\": \"https://tea-3.lozi.vn/v1/ship/resized/heekcaa-by-royaltea-quan-hoan-kiem-ha-noi-6-1-1514629236?w=320&type=o\"\n   },\n   {\n       \"id\": 4002,\n       \"name\": \"Trà ô long mộc hoa kem cheese\",\n       \"idFoodCategory\": 2,\n       \"idRestaurant\": 25,\n       \"price\": 49000,\n       \"sold\": 263,\n       \"image\": \"https://media.foody.vn/res/g71/707215/s570x570/2017123011210-2017127114251-7.-tra-olong-dao-kem-cheese.jpg\"\n   }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          }
        ]
      }
    },
    "filename": "app/router/food.js",
    "groupTitle": "Food",
    "name": "GetFoodSearchfood"
  },
  {
    "version": "2.0.1",
    "type": "get",
    "url": "/food/searchtype",
    "title": "Search food by name of category",
    "group": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryname",
            "description": "<p>name of category</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "[\n\n   {\n       \"id\": 4005,\n       \"name\": \"Socola cheese\",\n       \"idFoodCategory\": 2,\n       \"idRestaurant\": 22,\n       \"price\": 53000,\n       \"sold\": 385,\n       \"image\": \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbvPI9ZcVSBWKIe0ziwVqbfBYoVfBEljQYTwVKuh5sMd42Z9mWxg\"\n   },\n   {\n       \"id\": 4009,\n       \"name\": \"Trà dâu Nam Mỹ chanh vàng\",\n       \"idFoodCategory\": 2,\n       \"idRestaurant\": 22,\n       \"price\": 50000,\n       \"sold\": 263,\n       \"image\": \"https://tea-3.lozi.vn/v1/ship/resized/heekcaa-by-royaltea-quan-hoan-kiem-ha-noi-6-1-1514629236?w=320&type=o\"\n   }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          }
        ]
      }
    },
    "filename": "app/router/food.js",
    "groupTitle": "Food",
    "name": "GetFoodSearchtype"
  },
  {
    "version": "2.0.3",
    "type": "get",
    "url": "/getNoti",
    "title": "Get noti for user",
    "group": "Notification",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          },
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Header",
            "type": "application/json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "[\n\t{\n    \"id\": 1,\n    \"idUser\": 10,\n    \"title\": \"The Alley giảm giá 50%\",\n    \"image\": null,\n    \"content\": \"Nhân ngày 20/11 giảm giá 50% tất cả các chi nhánh của The Alley\"\n },\n\t{\n    \"id\": 2,\n    \"idUser\": 10,\n    \"title\": \"GongCha tri ân thầy cô\",\n    \"image\": null,\n    \"content\": \"Nhân ngày 20/11 Gong cha giảm giá 70% toàn bộ menu\"\n }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>unauthorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"unauthorized\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/router/notification.js",
    "groupTitle": "Notification",
    "name": "GetGetnoti"
  },
  {
    "version": "2.0.3",
    "type": "post",
    "url": "/deviceToken",
    "title": "Post device token",
    "group": "Notification",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          },
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Header",
            "type": "application/json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "deviceId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "platform",
            "description": "<p>(android/ios)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\t{\n    \"id\": 1,\n    \"idUser\": 10,\n    \"deviceId\": \"jahjakj\",\n    \"platform\": android,\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>unauthorized</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>other error</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"unauthorized\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/router/notification.js",
    "groupTitle": "Notification",
    "name": "PostDevicetoken"
  },
  {
    "version": "2.0.3",
    "type": "post",
    "url": "/pushNotificationtoAll",
    "title": "Send notification to all user",
    "group": "Notification",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Header",
            "type": "application/json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>failure</p>"
          }
        ]
      }
    },
    "filename": "app/router/notification.js",
    "groupTitle": "Notification",
    "name": "PostPushnotificationtoall"
  },
  {
    "version": "2.0.3",
    "type": "post",
    "url": "/pushNotificationtoUser",
    "title": "Send notification to user",
    "group": "Notification",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          },
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Header",
            "type": "application/json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>failure</p>"
          }
        ]
      }
    },
    "filename": "app/router/notification.js",
    "groupTitle": "Notification",
    "name": "PostPushnotificationtouser"
  },
  {
    "version": "2.0.1",
    "type": "get",
    "url": "/order/cancelOrder/:id",
    "title": "cancel order if order not delivery",
    "group": "Order",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\"msg\": \"Successfull\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessfull</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>unauthorized</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "402",
            "description": "<p>Can't cancel order</p>"
          }
        ]
      }
    },
    "filename": "app/router/order.js",
    "groupTitle": "Order",
    "name": "GetOrderCancelorderId"
  },
  {
    "version": "2.0.1",
    "type": "get",
    "url": "/order/getAll",
    "title": "get all history order of user",
    "group": "Order",
    "description": "<p>get all history order of user</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    {\n[\n{\n    \"id\": 30,\n    \"idUser\": 6,\n    \"totalPrice\": 100000,\n    \"date\": \"2018-10-13T15:36:57.745Z\",\n    \"address\": \"An dương vương, Phường Cầu Ông Lãnh, Quận 1\",\n    \"idRestaurant\": 1,\n    \"phone\": \"123456\",\n    \"status\": -1\n},\n    {..},\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessfull</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>unauthorized</p>"
          }
        ]
      }
    },
    "filename": "app/router/order.js",
    "groupTitle": "Order",
    "name": "GetOrderGetall"
  },
  {
    "version": "2.0.1",
    "type": "get",
    "url": "/order/getOrder/:id",
    "title": "get detail order by id",
    "group": "Order",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\"order\": {\n    \"id\": 28,\n    \"idUser\": 6,\n    \"totalPrice\": 100000,\n    \"date\": \"2018-10-13T10:20:37.448Z\",\n    \"address\": \"An dương vương, Phường Cầu Ông Lãnh, Quận 1\",\n    \"idRestaurant\": 1,\n    \"phone\": \"123456\",\n    \"status\": -1\n},\n\"details\": [\n   {\n       \"id\": 37,\n       \"idOrder\": 28,\n      \"idFood\": 1001,\n      \"quantity\": 1,\n     \"note\": \"aaa\"\n },\n {\n    \"id\": 38,\n    \"idOrder\": 28,\n    \"idFood\": 1002,\n    \"quantity\": 1,\n    \"note\": \"aaa\"\n}\n]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessfull</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>unauthorized</p>"
          }
        ]
      }
    },
    "filename": "app/router/order.js",
    "groupTitle": "Order",
    "name": "GetOrderGetorderId"
  },
  {
    "version": "2.0.1",
    "type": "post",
    "url": "/order/create",
    "title": "create order",
    "group": "Order",
    "description": "<p>create order</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "totalPrice",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Street",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "idDistrict",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "idWard",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "idRestaurant",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "item",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "    {\n\"totalPrice\":\"100000\",\n\"Street\":\"An dương vương\",\n\"idWard\":\"9223\",\n\"idDistrict\":\"13096\",\n\"phone\":\"123456\",\n\"idRestaurant\": \"1\",\n\"item\":[\n\t{\n\t\t\"idFood\":\"1001\",\n\t\t\"quantity\":\"1\",\n\t\t\"note\":\"aaa\"\n\t},\n\t{\n\t\t\"idFood\":\"1002\",\n\t\t\"quantity\":\"1\",\n\t\t\"note\":\"aaa\"\n\t}\n]\n}",
        "type": "json"
      }
    ],
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessfull</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>unauthorized</p>"
          }
        ]
      }
    },
    "filename": "app/router/order.js",
    "groupTitle": "Order",
    "name": "PostOrderCreate"
  },
  {
    "version": "2.0.1",
    "type": "post",
    "url": "/order/create/v2",
    "title": "create order version 2",
    "group": "Order",
    "description": "<p>create order</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "totalPrice",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "idRestaurant",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "note",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Object[]",
            "optional": false,
            "field": "item",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "{\n\"totalPrice\":\"100000\",\n\"address\": \"An dương Vương\",\n\"phone\":\"123456\",\n\"idRestaurant\": \"1\",\n\"item\":[\n\t{\n\t\t\"idFood\":\"1001\",\n\t\t\"quantity\":\"1\",\n\t\t\"note\":\"aaa\"\n\t},\n\t{\n\t\t\"idFood\":\"1002\",\n\t\t\"quantity\":\"1\",\n\t\t\"note\":\"aaa\"\n\t}\n]\n}",
        "type": "json"
      }
    ],
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessfull</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>unauthorized</p>"
          }
        ]
      }
    },
    "filename": "app/router/order.js",
    "groupTitle": "Order",
    "name": "PostOrderCreateV2"
  },
  {
    "version": "2.2.0",
    "type": "get",
    "url": "/restaurant/getAll/:per_page&:page",
    "title": "Get restaurant by page",
    "group": "Restaurant",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "per_page",
            "description": "<p>Number of result per page</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "page",
            "description": "<p>Number of page</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "itemCount",
            "description": "<p>Number of record is returned</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>Array of restaurants</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "next_page",
            "description": "<p>Number of next page. If <code>next_page = -1</code>, no data to return</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\"itemCount\": 7,\n\"data\": [\n   {\n       \"info\": {\n           \"id\": 22,\n           \"name\": \"Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng\",\n           \"idAddress\": 1,\n           \"rating\": 3,\n           \"image\": \"https://images.foody.vn/res/g66/656352/prof/s576x330/foody-mobile-ll-jpg.jpg\",\n           \"timeOpen\": null,\n           \"timeClose\": null,\n           \"feeShip': 5000\n       },\n       \"address\": {}\n   },\n   {\n       \"info\": {\n           \"id\": 23,\n           \"name\": \"Hanuri - Quán Ăn Hàn Quốc - Nguyễn Đình Chiểu\",\n           \"idAddress\": 2,\n           \"rating\": 5,\n           \"image\": \"https://images.foody.vn/res/g5/47168/prof/s576x330/foody-mobile-hx7sjiba-jpg-303-636149797060125332.jpg\",\n           \"timeOpen\": null,\n           \"timeClose\": null\n           \"feeShip': 6000\n       },\n       \"address\": {}\n   },\n\t],\n\t\"next_page\": -1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          }
        ]
      }
    },
    "filename": "app/router/restaurant.js",
    "groupTitle": "Restaurant",
    "name": "GetRestaurantGetallPer_pagePage"
  },
  {
    "version": "2.2.0",
    "type": "get",
    "url": "/restaurant/getAllCommentById/:id",
    "title": "Get all comments",
    "group": "Restaurant",
    "description": "<p>Get all comments of a restaurant by id of restaurant</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "idRestaurant",
            "description": "<p>id of restaurant</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "result",
            "description": "<p>array of comment (has been sorted by time comment)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"idRestaurant\": \"2\",\n  \"result\": [\n      {\n          \"id\": 9,\n          \"idRestaurant\": 2,\n          \"name\": \"Đông Phương Bất Bại\",\n          \"content\": \"Cay chưa đã, các món nên cay thêm nữa\",\n          \"createAt\": \"2018-12-18T15:29:29.829Z\"\n      },\n      {\n          \"id\": 3,\n          \"idRestaurant\": 2,\n          \"name\": \"Trương Vô Kỵ\",\n          \"content\": \"menu xịn sò, toàn đồ sang chảnh\",\n          \"createAt\": \"2018-12-18T11:20:33.000Z\"\n      },\n      {\n          \"id\": 4,\n          \"idRestaurant\": 2,\n          \"name\": \"Vi Tiểu Bảo\",\n          \"content\": \"phục vụ nhanh, nhiều món, món nào cũng muốn chọn\",\n          \"createAt\": \"2018-12-17T15:21:32.000Z\"\n      },\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-401:",
          "content": "{ \n     msg: 'Restaurant does not exist' \n}",
          "type": "json"
        }
      ],
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>restaurant does not exist</p>"
          }
        ]
      }
    },
    "filename": "app/router/restaurant.js",
    "groupTitle": "Restaurant",
    "name": "GetRestaurantGetallcommentbyidId"
  },
  {
    "version": "2.2.0",
    "type": "get",
    "url": "/restaurant/getCategory/:idCategory",
    "title": "Get restaurant by category",
    "group": "Restaurant",
    "description": "<p>Get all restaurants with corresponding categories attached to the menu</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idCategory",
            "description": "<p>ID of category</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The Restaurant-ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>The Restaurant-Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "idAddress",
            "description": "<p>The id of Restaurant-Address</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>The rating of Reastaurant</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>URL of source's image</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "timeOpen",
            "description": "<p>Time open of Reastaurant</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "timeClose",
            "description": "<p>Time close of Reastaurant</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "FOOD",
            "description": "<p>Array of food that belong category</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "[\n\t{\n   \"id\": 22,\n  \"name\": \"Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng\",\n   \"idAddress\": null,\n  \"rating\": 3,\n  \"image\": null,\n  \"timeOpen\": null,\n  \"timeClose\": null,\n  \"feeShip\": 5000\n},\n{\n   \"id\": 24,\n   \"name\": \"GAXEO Chicken - Beer & BBQ - Hồ Tùng Mậu\",\n  \"idAddress\": null,\n  \"rating\": 4,\n  \"image\": null,\n  \"timeOpen\": null,\n  \"timeClose\": null,\n  \"feeShip\": 5000\n}\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          }
        ]
      }
    },
    "filename": "app/router/restaurant.js",
    "groupTitle": "Restaurant",
    "name": "GetRestaurantGetcategoryIdcategory"
  },
  {
    "version": "2.2.0",
    "type": "get",
    "url": "/restaurant/getFirst3CommentById/:id",
    "title": "Get first 3 comments",
    "group": "Restaurant",
    "description": "<p>Get first 3 comments of a restaurant by id of restaurant</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "idRestaurant",
            "description": "<p>id of restaurant</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "result",
            "description": "<p>array of comment (has been sorted by time comment)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"idRestaurant\": \"2\",\n  \"result\": [\n      {\n          \"id\": 9,\n          \"idRestaurant\": 2,\n          \"name\": \"Đông Phương Bất Bại\",\n          \"content\": \"Cay chưa đã, các món nên cay thêm nữa\",\n          \"createAt\": \"2018-12-18T15:29:29.829Z\"\n      },\n      {\n          \"id\": 3,\n          \"idRestaurant\": 2,\n          \"name\": \"Trương Vô Kỵ\",\n          \"content\": \"menu xịn sò, toàn đồ sang chảnh\",\n          \"createAt\": \"2018-12-18T11:20:33.000Z\"\n      },\n      {\n          \"id\": 4,\n          \"idRestaurant\": 2,\n          \"name\": \"Vi Tiểu Bảo\",\n          \"content\": \"phục vụ nhanh, nhiều món, món nào cũng muốn chọn\",\n          \"createAt\": \"2018-12-17T15:21:32.000Z\"\n      },\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-401:",
          "content": "{ \n     msg: 'Restaurant does not exist' \n}",
          "type": "json"
        }
      ],
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>restaurant does not exist</p>"
          }
        ]
      }
    },
    "filename": "app/router/restaurant.js",
    "groupTitle": "Restaurant",
    "name": "GetRestaurantGetfirst3commentbyidId"
  },
  {
    "version": "2.2.0",
    "type": "get",
    "url": "/restaurant/getMenu/:id",
    "title": "Get menu of restaurant",
    "group": "Restaurant",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The id of the restaurant you want to get the menu</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "menu",
            "description": "<p>Array of food</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\"resource\": [\n  {\n      \"url\": 'https://thumbor.mumu.agency/unsafe/1000x562/https://www.theransomnote.com/media/articles/rare-african-music-tops-trendbases-restaurant-background-music-charts/4ca464fe-54ae-457f-8680-702aaa8a13ab.jpg',\n      \"type\": 'image'\n  },\n  {\n      \"url\": 'https://www.youtube.com/embed/AK8nlF6phjY',\n      \"type\": 'video'     \n  },\n  {\n      \"url\": 'https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1363450/910/607/m1/fpnw/wm0/img_4185-.jpg?1465877099&s=d4891a812f1982d1db02d86dd507fe43',\n      \"type\": 'image'\n  },\n],\n\n\"restaurant\": {\n   \"id\": 22,\n    \"name\": \"Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng\",\n    \"idAddress\": 1,\n    \"rating\": 3,\n    \"image\": \"https://images.foody.vn/res/g66/656352/prof/s576x330/foody-mobile-ll-jpg.jpg\",\n    \"timeOpen\": null,\n    \"timeClose\": null\n    \"feeShip\": 5000\n},\n\"address\": {\n   \"address\": \"Số 8 huỳnh thúc kháng, Phường Bến Nghé,Quận 1\",\n   \"latitude\": 10.773533,\n   \"longitude\": 106.702899\n    },\n\"menu\": [\n    {\n        \"id\": 1003,\n        \"name\": \"Socola cheese\",\n        \"idFoodCategory\": 2,\n        \"idRestaurant\": 22,\n        \"image\": \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbvPI9ZcVSBWKIe0ziwVqbfBYoVfBEljQYTwVKuh5sMd42Z9mWxg\",\n        \"price\": 53000,\n        \"sold\": 385\n    },\n    {\n        \"id\": 1004,\n        \"name\": \"Trà dâu Nam Mỹ chanh vàng\",\n        \"idFoodCategory\": 2,\n        \"idRestaurant\": 22,\n        \"image\": \"https://tea-3.lozi.vn/v1/ship/resized/heekcaa-by-royaltea-quan-hoan-kiem-ha-noi-6-1-1514629236?w=320&type=o\",\n        \"price\": 50000,\n        \"sold\": 263\n    },\n    {\n        \"id\": 1005,\n        \"name\": \"Hồng trà Heekcaa cheese\",\n        \"idFoodCategory\": 2,\n        \"idRestaurant\": 22,\n        \"image\": \"http://cdn01.diadiemanuong.com/ddau/640x/thu-cho-bang-duoc-tra-sua-heekcaa-uu-dai-20-cho-5-mon-moi-60af04a6636451490887596264.jpg\",\n        \"price\": 49000,\n        \"sold\": 245\n    },\n \n]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          }
        ]
      }
    },
    "filename": "app/router/restaurant.js",
    "groupTitle": "Restaurant",
    "name": "GetRestaurantGetmenuId"
  },
  {
    "version": "2.2.0",
    "type": "get",
    "url": "/restaurant/nearMe/:lat&:long",
    "title": "Get restaurant near me",
    "group": "Restaurant",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "array",
            "description": "<p>of restaurant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "[\n{\n    \"id\": 1,\n    \"idDistrict\": 13096,\n    \"idWard\": 9219,\n    \"street\": \"Số 8 huỳnh thúc kháng\",\n    \"latitude\": 10.773533,\n    \"longitude\": 106.702899,\n    \"RESTAURANT\": {\n        \"id\": 1,\n        \"name\": \"Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng\",\n        \"idAddress\": 1,\n        \"rating\": 4,\n        \"image\": \"https://images.foody.vn/res/g66/656352/prof/s576x330/foody-mobile-ll-jpg.jpg\",\n        \"timeOpen\": null,\n        \"timeClose\": null\n        \"feeShip\": 5000\n    }\n}\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          }
        ]
      }
    },
    "filename": "app/router/restaurant.js",
    "groupTitle": "Restaurant",
    "name": "GetRestaurantNearmeLatLong"
  },
  {
    "version": "2.2.0",
    "type": "get",
    "url": "/restaurant/search",
    "title": "Search restaurant by name",
    "group": "Restaurant",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of restaurant</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The Restaurant-ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>The Restaurant-Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "idAddress",
            "description": "<p>The id of Restaurant-Address</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rating",
            "description": "<p>The rating of Reastaurant</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>URL of source's image</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "timeOpen",
            "description": "<p>Time open of Reastaurant</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "timeClose",
            "description": "<p>Time close of Reastaurant</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "[\n{\n   \"id\": 22,\n   \"name\": \"Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng\",\n   \"idAddress\": null,\n   \"rating\": 3,\n   \"image\": null,\n   \"timeOpen\": null,\n   \"timeClose\": null,\n   \"feeShip\": 5000\n}\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unsuccessful</p>"
          }
        ]
      }
    },
    "filename": "app/router/restaurant.js",
    "groupTitle": "Restaurant",
    "name": "GetRestaurantSearch"
  },
  {
    "version": "2.2.0",
    "type": "post",
    "url": "/restaurant/addComment",
    "title": "Add comment",
    "group": "Restaurant",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>the name of the commentator</p>"
          },
          {
            "group": "Parameter",
            "type": "Nummber",
            "optional": false,
            "field": "idRestaurant",
            "description": "<p>idRestaurant id of the restaurant want to comment on</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>content of comment</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p><code>&quot;Have commented successfully&quot;</code> if <code>statuscode = 200</code>, <code>&quot;Restaurant does not exist&quot;</code> if <code>statuscode = 401</code> and <code>&quot;Params invalid&quot;</code> if <code>statuscode = 402</code></p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>(if <code>statuscode = 200</code>) infomation of comments</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"msg\": \"Have commented successfully\",\n  \"result\": {\n      \"id\": 1,\n      \"idRestaurant\": 1,\n      \"content\": \"Ngon quá\",\n      \"name\": \"Trương Vô Kỵ\",\n      \"createAt\": \"2018-12-18T15:29:29.829Z\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Example-Obj to send:",
          "content": "{\n  \"name\": \"Trương Vô Kỵ\",\n  \"idRestaurant\": \"1\",\n  \"content\": \"Ngon quá\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>other error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>Restaurant does not exist</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "402",
            "description": "<p>Params invalid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-401:",
          "content": "{\n  \"msg\": \"Restaurant does not exist\"\n}",
          "type": "json"
        },
        {
          "title": "Error-402:",
          "content": "{\n  \"msg\": \"Params invalid\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/router/restaurant.js",
    "groupTitle": "Restaurant",
    "name": "PostRestaurantAddcomment"
  },
  {
    "version": "2.2.0",
    "type": "get",
    "url": "/getinfo",
    "title": "Get info user",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          },
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Header",
            "type": "application/json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    {\n      \"email\": \"ptthao.itus@gmail.com\"\n      \"phone\": \"12345678\"\n       \"address\": {\n\t\t\t\"id\": 2,\n\t\t\t\"idDistrict\": 13096,\n\t\t\t\"idWard\": 9219,\n\t\t\t\"street\": \"Trần Phú\",\n\t\t\t\"latitude\": 10.775738,\n\t\t\t\"longitude\": 106.687187\n\t\t},\n      \t\"userName\": \"Phan Thao\"\n \t   \"avatarUrl\": \"https://food-delivery-server.herokuapp.com/2.jpg\",\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>unauthorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"unauthorized\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "GetGetinfo"
  },
  {
    "version": "2.2.0",
    "type": "get",
    "url": "/getinfo/v2",
    "title": "Get info user v2",
    "group": "User",
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "GetGetinfoV2"
  },
  {
    "version": "2.0.1",
    "type": "post",
    "url": "/forgetPassword",
    "title": "Forget password",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Email",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>reset password successful and sent email</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"message\": \"Reset password successfully, an email has been sent to:  -> user email =  phuongthao29100@gmail.com\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>email is wrong</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "500",
            "description": "<p>other error</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"message\": \"email is wrong\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PostForgetpassword"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>Login successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    {\n      \"msg\": \"Login successful\",\n\t\t \"token\": \"46sd5f46s5df4s68df6sd5f4s65d4fs65df4s65df46sd5f46s\",\n\t\t \"id\": \"1\",\t\n\t\t \"email\": \"abc@gmail.com\",\n\t\t \"phone\": \"0123456789\",\n\t\t \"avatarUrl\": \"https://food-delivery-server.herokuapp.com/2.jpg\",\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>Email or Password is wrong</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>Account not verified</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"Email or Password is wrong\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"Account not verified \"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PostLogin"
  },
  {
    "type": "post",
    "url": "/login/v2",
    "title": "Login version2",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PostLoginV2"
  },
  {
    "type": "post",
    "url": "/register",
    "title": "Register",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"id\": 32\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>Email already exists</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>Invalid email</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "500",
            "description": "<p>other error</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"Email already exists\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"Invalid email\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"Email already exists\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PostRegister"
  },
  {
    "type": "post",
    "url": "/register/v2",
    "title": "Register version 2",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userName",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"id\": 32\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>Email already exists</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>Invalid email</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "500",
            "description": "<p>other error</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"Email already exists\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"Invalid email\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"Email already exists\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PostRegisterV2"
  },
  {
    "version": "2.2.0",
    "type": "post",
    "url": "/updateInfo",
    "title": "Update info user",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          },
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Header",
            "type": "application/json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "idDistrict",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "idWard",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "street",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userName",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>Update succesfull</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"msg\" : \"Update succesfull\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>orther error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>unauthorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"unauthorized\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PostUpdateinfo"
  },
  {
    "version": "2.0.2",
    "type": "post",
    "url": "/updatePassword",
    "title": "Update password",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          },
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Header",
            "type": "application/json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>Update succesfull</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"msg\" : \"Update succesfull\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>orther error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "401",
            "description": "<p>unauthorized</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"unauthorized\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PostUpdatepassword"
  },
  {
    "type": "post",
    "url": "/verify",
    "title": "Verify user if user want to send email again",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>email user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    {\n\t\tmsg : \"Successfull\"\n\t  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "500",
            "description": "<p>other error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>Email verifie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"Email verified\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PostVerify"
  },
  {
    "version": "2.2.0",
    "type": "put",
    "url": "/updateAvatar",
    "title": "Update avatar",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "bearer",
            "optional": false,
            "field": "Token",
            "description": ""
          },
          {
            "group": "Header",
            "type": "Content-Type",
            "optional": false,
            "field": "Key",
            "description": ""
          },
          {
            "group": "Header",
            "type": "application/json",
            "optional": false,
            "field": "Value",
            "description": ""
          }
        ]
      }
    },
    "description": "<p>Update avatar.<br> The input argument is a formdata.<br></p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "FormData",
            "optional": false,
            "field": "FormData",
            "description": "<p>FormData contains image</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Param-Example:",
          "content": "let data = new FormData();\ndata.append('file', {\n\turi: newData.image.uri,\n\ttype: 'image/jpeg',\n\tname: 'teste'\n})",
          "type": "json"
        },
        {
          "title": "Request-Example:uri",
          "content": "axios.put('/endpoint/url', data, config)\n\t.then(res => console.log(res))\n\t.catch(err => console.log(err))\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "statuscode",
            "optional": false,
            "field": "200",
            "description": "<p>Update succesfull</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  \"avatarUrl\": \"https://food-delivery-server.herokuapp.com/2.jpg\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "statuscode",
            "optional": false,
            "field": "400",
            "description": "<p>orther error</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"msg\": \"unauthorized\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PutUpdateavatar"
  },
  {
    "version": "2.2.0",
    "type": "put",
    "url": "/updateAvatar/v2",
    "title": "Update avatar v2",
    "group": "User",
    "filename": "app/router/user.js",
    "groupTitle": "User",
    "name": "PutUpdateavatarV2"
  }
] });
