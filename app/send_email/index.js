var smtpConfig = require('../../setting').smtpConfig;
var nodemailer = require('nodemailer');
var smtpTransport = nodemailer.createTransport(smtpConfig);

module.exports = smtpTransport;