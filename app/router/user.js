const user = require('../controller/user');
let multer = require('multer');
let upload = multer(); //setting the default folder for multer
module.exports = function (app, passport) {
	/**
	 * @api {post} /register Register
	 * @apiGroup User
	 * @apiParam {string} email
	 * @apiParam {string} password
	 *
	 * @apiSuccess {statuscode} 200 successful
	 * @apiSuccessExample {json} Success-Response:
	 *     {
	 *       "id": 32
	 *     }
	 *
	 * @apiError {statuscode} 400 Email already exists
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "Email already exists"
	 *     }
	 * @apiError {statuscode} 401 Invalid email
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "Invalid email"
	 *     }
	 * @apiError {statuscode} 500 other error
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "Email already exists"
	 *     }
	 */
	app.post('/register', user.create);
	/**
	 * @api {post} /register/v2 Register version 2
	 * @apiGroup User
	 * @apiParam {string} email
	 * @apiParam {string} password
	 * @apiParam {string} phone
	 * @apiParam {string} userName
	 *
	 * @apiSuccess {statuscode} 200 successful
	 * @apiSuccessExample {json} Success-Response:
	 *     {
	 *       "id": 32
	 *     }
	 *
	 * @apiError {statuscode} 400 Email already exists
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "Email already exists"
	 *     }
	 * @apiError {statuscode} 401 Invalid email
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "Invalid email"
	 *     }
	 * @apiError {statuscode} 500 other error
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "Email already exists"
	 *     }
	 */
	app.post('/register/v2', user.createv2);
	/**
   * @apiVersion 2.2.0
	 * @api {get} /getinfo Get info user
	 * @apiGroup User
   * @apiHeader {bearer} Token
   * @apiHeader {Content-Type} Key
	 * @apiHeader {application/json} Value
	 *
	 * @apisuccess {statuscode} 200 successful
	 * @apisuccessExample {json} Success-Response:
	 *     {
	 *       "email": "ptthao.itus@gmail.com"
	 *       "phone": "12345678"
	 *        "address": {
	*			"id": 2,
	*			"idDistrict": 13096,
	*			"idWard": 9219,
	*			"street": "Trần Phú",
	*			"latitude": 10.775738,
	*			"longitude": 106.687187
	*		},
   *       	"userName": "Phan Thao"
	 *  	   "avatarUrl": "https://food-delivery-server.herokuapp.com/2.jpg",
	 *     }
	 *
	 * @apiError {statuscode} 400 unauthorized
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "unauthorized"
	 *     }
	 */
	app.get('/getinfo/v2',user.findById2)
	/** 
	* @apiVersion 2.2.0
	 * @api {get} /getinfo/v2 Get info user v2
	 * @apiGroup User
	 */
	app.get('/getinfo', user.findById);
	/**
	 * @apiVersion 2.0.2
	 * @api {post} /updatePassword Update password
	 * @apiGroup User
	 * @apiHeader {bearer} Token
	 * @apiHeader {Content-Type} Key
	 * @apiHeader {application/json} Value
	   *
	   * @apiParam {string} password
	 *
	 * @apisuccess {statuscode} 200 Update succesfull
	 * @apisuccessExample {json} Success-Response:
	 *     {
	 *       "msg" : "Update succesfull"
	 *     }
	 *
	   * @apiError {statuscode} 400 orther error
	 * @apiError {statuscode} 401 unauthorized
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "unauthorized"
	 *     }
	 */
	app.post('/updatePassword', user.updatePassword);
	/**
	 * @apiVersion 2.2.0
	 * @api {post} /updateInfo Update info user
	 * @apiGroup User
	 * @apiHeader {bearer} Token
	 * @apiHeader {Content-Type} Key
	 * @apiHeader {application/json} Value
	 *
	 * @apiParam {string} phone
	 * @apiParam {number} idDistrict
	 * @apiParam {number} idWard
	 * @apiParam {string} street
   * @apiParam {string} userName
	 *
	 * @apisuccess {statuscode} 200 Update succesfull
	 * @apisuccessExample {json} Success-Response:
	 *     {
	 *       "msg" : "Update succesfull"
	 *     }
	 *
	 * @apiError {statuscode} 400 orther error
	 * @apiError {statuscode} 401 unauthorized
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "unauthorized"
	 *     }
	 */
	app.post('/updateInfo', user.updateInfo);
	/**
	 * @api {post} /login Login
	 * @apiGroup User
	 * @apiParam {string} email
	 * @apiParam {string} password
	 *
	 * @apisuccess {statuscode} 200 Login successful
	 * @apisuccessExample {json} Success-Response:
	 *     {
	 *       "msg": "Login successful",
	 * 		 "token": "46sd5f46s5df4s68df6sd5f4s65d4fs65df4s65df46sd5f46s",
	 *		 "id": "1",	
	 *		 "email": "abc@gmail.com",
	 *		 "phone": "0123456789",
	 *		 "avatarUrl": "https://food-delivery-server.herokuapp.com/2.jpg",
	 *     }
	 *
	 * @apiError {statuscode} 400 Email or Password is wrong
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "Email or Password is wrong"
	 *     }
	 * @apiError {statuscode} 401 Account not verified
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "Account not verified "
	 *     }
	 */
	app.post('/login', user.login);
	/**
	 * @api {post} /login/v2 Login version2 
	 * @apiGroup User
	 * @apiParam {string} email
	 * @apiParam {string} password
	 */
	app.post('/login/v2', user.loginv2);
	/**
	* @apiVersion 2.0.1
	* @api {post} /forgetPassword Forget password
	* @apiGroup User
	* @apiParam {string} Email
	* @apiSuccess {statuscode} 200 reset password successful and sent email
	* @apiSuccessExample {json} Success-Response:
	*     {
	*       "message": "Reset password successfully, an email has been sent to:  -> user email =  phuongthao29100@gmail.com"
	*     }
	*
	* @apiError {statuscode} 400 email is wrong
	* @apiErrorExample {json} Error-Response:
	*     {
	*       "message": "email is wrong"
	*     }
	* @apiError {statuscode} 500 other error
	*/
	app.post('/forgetPassword', user.forgetPassword);
	/**
	* @api {post} /verify Verify user if user want to send email again
	* @apiGroup User
	* @apiParam {string} email email user
	*
	* @apiSuccess {statuscode} 200 successful
	* @apiSuccessExample {json} Success-Response:
	*     {
	*		msg : "Successfull"
	* 	  }
	*
	* @apiError {statuscode} 500 other error
	* @apiError {statuscode} 400 Email verifie
	* @apiErrorExample {json} Error-Response:
	*     {
	*       "msg": "Email verified"
	*     }
	*/
	app.post('/verify', user.verify);

	/**
	 * @apiVersion 2.2.0
	 * @api {put} /updateAvatar Update avatar
	 * @apiGroup User
	 * @apiHeader {bearer} Token
	 * @apiHeader {Content-Type} Key
	 * @apiHeader {application/json} Value
	 * @apiDescription Update avatar.<br>
	 *	The input argument is a formdata.<br>
	 * 
	 * @apiParam {FormData} FormData FormData contains image
	 ** @apiParamExample {json} Param-Example:
	 *		let data = new FormData();
	 *		data.append('file', {
	 *			uri: newData.image.uri,
	 *			type: 'image/jpeg',
	 *			name: 'teste'
	 *		})
	 * @apiParamExample {json} Request-Example:uri
	 *		axios.put('/endpoint/url', data, config)
	 *			.then(res => console.log(res))
	 *			.catch(err => console.log(err))
	 *		}
	 * @apisuccess {statuscode} 200 Update succesfull
	 * @apisuccessExample {json} Success-Response:
	 *     {
	 *       "avatarUrl": "https://food-delivery-server.herokuapp.com/2.jpg",
	 *     }
	 *
	 * @apiError {statuscode} 400 orther error
	 * @apiErrorExample {json} Error-Response:
	 *     {
	 *       "msg": "unauthorized"
	 *     }
	 */
	app.put('/updateAvatar', upload.single('file'), user.updateAvatar);
	app.post('/verify', user.verify);

	/**
	 * @apiVersion 2.2.0
	 * @api {put} /updateAvatar/v2 Update avatar v2
	 * @apiGroup User
	 */
	app.put('/updateAvatar/v2', upload.single('file'), user.updateAvatar2);
	app.get('/verify', user.verifyUser);
	app.get('/delete/:id', user.delete);
	app.get('/getAllUser', user.findAllUser);
};