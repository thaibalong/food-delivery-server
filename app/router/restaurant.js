const restaurant = require('../controller/restaurant');
const food = require('../controller/food');
const foodCate = require('../controller/food_categories');
module.exports = function (app, passport) {

    app.post('/restaurant/create', restaurant.create); //ok
	/**
   * @apiVersion 2.2.0
	 * @api {get} /restaurant/getAll/:per_page&:page Get restaurant by page
	 * @apiGroup Restaurant
   * @apiParam {number} per_page Number of result per page
   * @apiParam {number} page Number of page
	 *
	 * @apisuccess {statuscode} 200 successful
	 * @apisuccess {Number} itemCount Number of record is returned
	 * @apisuccess {Array} data Array of restaurants
	 * @apisuccess {Number} next_page Number of next page. If <code>next_page = -1</code>, no data to return
	 * @apisuccessExample {json} Success-Response:
	 *{
    * "itemCount": 7,
    *"data": [
    *    {
    *        "info": {
    *            "id": 22,
    *            "name": "Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng",
    *            "idAddress": 1,
    *            "rating": 3,
    *            "image": "https://images.foody.vn/res/g66/656352/prof/s576x330/foody-mobile-ll-jpg.jpg",
    *            "timeOpen": null,
    *            "timeClose": null,
	  *            "feeShip': 5000
    *        },
    *        "address": {}
    *    },
    *    {
    *        "info": {
    *            "id": 23,
    *            "name": "Hanuri - Quán Ăn Hàn Quốc - Nguyễn Đình Chiểu",
    *            "idAddress": 2,
    *            "rating": 5,
    *            "image": "https://images.foody.vn/res/g5/47168/prof/s576x330/foody-mobile-hx7sjiba-jpg-303-636149797060125332.jpg",
    *            "timeOpen": null,
    *            "timeClose": null
	  *            "feeShip': 6000
    *        },
    *        "address": {}
    *    },
	* 	],
    *	"next_page": -1
	*}
	 * @apiError {statuscode} 400 unsuccessful
	 */
    app.get('/restaurant/getAll/:per_page&:page', restaurant.getAllMerchants);   //need to test and update docs

	/**
	 * @apiVersion 2.2.0
	 * @api {get} /restaurant/getMenu/:id Get menu of restaurant
	 * @apiGroup Restaurant
	 * @apiParam {Number} id The id of the restaurant you want to get the menu
	 * @apisuccess {statuscode} 200 successful
	 * @apisuccess {Array} menu Array of food
	 * @apisuccessExample {json} Success-Response:
	 *{
    * "resource": [
    *   {
    *       "url": 'https://thumbor.mumu.agency/unsafe/1000x562/https://www.theransomnote.com/media/articles/rare-african-music-tops-trendbases-restaurant-background-music-charts/4ca464fe-54ae-457f-8680-702aaa8a13ab.jpg',
    *       "type": 'image'
    *   },
    *   {
    *       "url": 'https://www.youtube.com/embed/AK8nlF6phjY',
    *       "type": 'video'     
    *   },
    *   {
    *       "url": 'https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1363450/910/607/m1/fpnw/wm0/img_4185-.jpg?1465877099&s=d4891a812f1982d1db02d86dd507fe43',
    *       "type": 'image'
    *   },
    * ],
    *
    * "restaurant": {
    *    "id": 22,
    *     "name": "Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng",
    *     "idAddress": 1,
    *     "rating": 3,
    *     "image": "https://images.foody.vn/res/g66/656352/prof/s576x330/foody-mobile-ll-jpg.jpg",
    *     "timeOpen": null,
    *     "timeClose": null
	  *     "feeShip": 5000
    * },
    * "address": {
    *    "address": "Số 8 huỳnh thúc kháng, Phường Bến Nghé,Quận 1",
    *    "latitude": 10.773533,
    *    "longitude": 106.702899
    },
    * "menu": [
    *     {
    *         "id": 1003,
    *         "name": "Socola cheese",
    *         "idFoodCategory": 2,
    *         "idRestaurant": 22,
    *         "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbvPI9ZcVSBWKIe0ziwVqbfBYoVfBEljQYTwVKuh5sMd42Z9mWxg",
    *         "price": 53000,
    *         "sold": 385
    *     },
    *     {
    *         "id": 1004,
    *         "name": "Trà dâu Nam Mỹ chanh vàng",
    *         "idFoodCategory": 2,
    *         "idRestaurant": 22,
    *         "image": "https://tea-3.lozi.vn/v1/ship/resized/heekcaa-by-royaltea-quan-hoan-kiem-ha-noi-6-1-1514629236?w=320&type=o",
    *         "price": 50000,
    *         "sold": 263
    *     },
    *     {
    *         "id": 1005,
    *         "name": "Hồng trà Heekcaa cheese",
    *         "idFoodCategory": 2,
    *         "idRestaurant": 22,
    *         "image": "http://cdn01.diadiemanuong.com/ddau/640x/thu-cho-bang-duoc-tra-sua-heekcaa-uu-dai-20-cho-5-mon-moi-60af04a6636451490887596264.jpg",
    *         "price": 49000,
    *         "sold": 245
    *     },
    *  
    * ]
	*}
	 *
	 * @apiError {statuscode} 400 unsuccessful
	 */
    app.get('/restaurant/getMenu/:id', restaurant.getMerchant);  //ok
	/**
   * @apiVersion 2.2.0
	 * @api {get} /restaurant/search Search restaurant by name
	 * @apiGroup Restaurant
     * @apiParam {String} name Name of restaurant
	 *
	 * @apisuccess {statuscode} 200 successful
	 * @apiSuccess {Number} id The Restaurant-ID
	 * @apiSuccess {String} name The Restaurant-Name
	 * @apiSuccess {Number} idAddress The id of Restaurant-Address
	 * @apiSuccess {Number} rating The rating of Reastaurant
	 * @apiSuccess {String} image URL of source's image
	 * @apiSuccess {Date} timeOpen Time open of Reastaurant
	 * @apiSuccess {Date} timeClose Time close of Reastaurant
	 * @apisuccessExample {json} Success-Response:
	 *[
    *{
    *    "id": 22,
    *    "name": "Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng",
    *    "idAddress": null,
    *    "rating": 3,
    *    "image": null,
    *    "timeOpen": null,
    *    "timeClose": null,
	 	*    "feeShip": 5000
    *}
	*]
	 *
	 * @apiError {statuscode} 400 unsuccessful
	 */
    app.get('/restaurant/search', restaurant.search);
    app.delete('/restaurant/deleteAll', restaurant.delete);
    //test

    app.get('/food/getCategory/:idCategory', food.getFoodCategory); //ok
	/**
	 * @apiVersion 2.2.0
	 * @api {get} /restaurant/getCategory/:idCategory Get restaurant by category
	 * @apiGroup Restaurant 
	 * @apiDescription Get all restaurants with corresponding categories attached to the menu
	 * @apiParam {Number} idCategory ID of category 
	 * @apiSuccess {statuscode} 200 successful
	 * @apiSuccess {Number} id The Restaurant-ID
	 * @apiSuccess {String} name The Restaurant-Name
	 * @apiSuccess {Number} idAddress The id of Restaurant-Address
	 * @apiSuccess {Number} rating The rating of Reastaurant
	 * @apiSuccess {String} image URL of source's image
	 * @apiSuccess {Date} timeOpen Time open of Reastaurant
	 * @apiSuccess {Date} timeClose Time close of Reastaurant
	 * @apiSuccess {Array} FOOD Array of food that belong category
	 * @apisuccessExample {json} Success-Response:
	 *[
     *	{
     *    "id": 22,
     *   "name": "Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng",
     *    "idAddress": null,
     *   "rating": 3,
     *   "image": null,
     *   "timeOpen": null,
     *   "timeClose": null,
	   *   "feeShip": 5000
   	 * },
   	 * {
     *    "id": 24,
     *    "name": "GAXEO Chicken - Beer & BBQ - Hồ Tùng Mậu",
     *   "idAddress": null,
     *   "rating": 4,
     *   "image": null,
     *   "timeOpen": null,
     *   "timeClose": null,
	   *   "feeShip": 5000
     *}
	 *]
	 * @apiError {statuscode} 400 unsuccessful
	 */
    app.get('/restaurant/getCategory/:idCategory', restaurant.getRestaurantByCategory); //ok

    app.post('/categories/create', foodCate.create);
	/**
	 * @apiVersion 2.0.0
	 * @api {get} /categories/getAll Get all categories
	 * @apiGroup Categories
	 *
	 * @apiSuccess {statuscode} 200 successful
	 * @apiSuccess {Number} id ID of category
	 * @apiSuccess {String} name Name of category 
	 * @apisuccessExample {json} Success-Response:
	 *[
	 *  {
	 *    "id": 1,
	 *    "name": "Cơm trưa",
     *     "image": "http://cdn.congso.net/asset/home/img/500/578de030ad67a_1468915760.jpg"
	 *  },
	 *  {
	 *    "id": 2,
	 *    "name": "Đồ uống"
     *  "image": "https://hd1.hotdeal.vn/images/94221_body_2.jpg"
	 *  },
	 *  {
	 *    "id": 3,
	 *    "name": "Đồ ăn"
     * image": "https://media.gody.vn//images/hinh-tong-hop/am-thuc-hue-gody/1-2018/32674114-20180103094505-hinh-tong-hop-am-thuc-hue-gody.jpg"
	 *  },
	 *]
	 * @apiError {statuscode} 400 unsuccessful
	 */
    app.get('/categories/getAll', foodCate.getAllCategories);

    /**
     * @apiVersion 2.2.0
     * @api {get} /restaurant/nearMe/:lat&:long Get restaurant near me
     * @apiGroup Restaurant
     *
     * @apiSuccess {statuscode} 200 successful
     * @apiSuccess {array} array of restaurant
     * @apisuccessExample {json} Success-Response:
     * [
      * {
      *     "id": 1,
      *     "idDistrict": 13096,
      *     "idWard": 9219,
      *     "street": "Số 8 huỳnh thúc kháng",
      *     "latitude": 10.773533,
      *     "longitude": 106.702899,
      *     "RESTAURANT": {
      *         "id": 1,
      *         "name": "Trà sữa Heekcaa Việt Nam - Huỳnh Thúc Kháng",
      *         "idAddress": 1,
      *         "rating": 4,
      *         "image": "https://images.foody.vn/res/g66/656352/prof/s576x330/foody-mobile-ll-jpg.jpg",
      *         "timeOpen": null,
      *         "timeClose": null
        *         "feeShip": 5000
      *     }
      * }
      * ]
     * @apiError {statuscode} 400 unsuccessful
     */
    app.get('/restaurant/nearMe/:lat&:long', restaurant.getRestaurantNearMe);

 /**
     * @apiVersion 2.2.0
     * @api {get} /restaurant/getAllCommentById/:id Get all comments
     * @apiGroup Restaurant
     * @apiDescription Get all comments of a restaurant by id of restaurant
     * @apiSuccess {Number} idRestaurant id of restaurant
     * @apiSuccess {Array} result array of comment (has been sorted by time comment)
     * 
     * @apisuccessExample {json} Success-Response:
     * {
    *   "idRestaurant": "2",
    *   "result": [
    *       {
    *           "id": 9,
    *           "idRestaurant": 2,
    *           "name": "Đông Phương Bất Bại",
    *           "content": "Cay chưa đã, các món nên cay thêm nữa",
    *           "createAt": "2018-12-18T15:29:29.829Z"
    *       },
    *       {
    *           "id": 3,
    *           "idRestaurant": 2,
    *           "name": "Trương Vô Kỵ",
    *           "content": "menu xịn sò, toàn đồ sang chảnh",
    *           "createAt": "2018-12-18T11:20:33.000Z"
    *       },
    *       {
    *           "id": 4,
    *           "idRestaurant": 2,
    *           "name": "Vi Tiểu Bảo",
    *           "content": "phục vụ nhanh, nhiều món, món nào cũng muốn chọn",
    *           "createAt": "2018-12-17T15:21:32.000Z"
    *       },
    *   ]
    *}
    *@apiErrorExample {json} Error-401:
     * { 
     *      msg: 'Restaurant does not exist' 
     * }
     * @apiError {statuscode} 400 unsuccessful
     * @apiError {statuscode} 401 restaurant does not exist
     */
    app.get('/restaurant/getAllCommentById/:id', restaurant.getAllCommentById);
/**
     * @apiVersion 2.2.0
     * @api {get} /restaurant/getFirst3CommentById/:id Get first 3 comments
     * @apiGroup Restaurant
     * @apiDescription Get first 3 comments of a restaurant by id of restaurant
     * @apiSuccess {Number} idRestaurant id of restaurant
     * @apiSuccess {Array} result array of comment (has been sorted by time comment)
     * 
     * @apisuccessExample {json} Success-Response:
     * {
    *   "idRestaurant": "2",
    *   "result": [
    *       {
    *           "id": 9,
    *           "idRestaurant": 2,
    *           "name": "Đông Phương Bất Bại",
    *           "content": "Cay chưa đã, các món nên cay thêm nữa",
    *           "createAt": "2018-12-18T15:29:29.829Z"
    *       },
    *       {
    *           "id": 3,
    *           "idRestaurant": 2,
    *           "name": "Trương Vô Kỵ",
    *           "content": "menu xịn sò, toàn đồ sang chảnh",
    *           "createAt": "2018-12-18T11:20:33.000Z"
    *       },
    *       {
    *           "id": 4,
    *           "idRestaurant": 2,
    *           "name": "Vi Tiểu Bảo",
    *           "content": "phục vụ nhanh, nhiều món, món nào cũng muốn chọn",
    *           "createAt": "2018-12-17T15:21:32.000Z"
    *       },
    *   ]
    *}
    *@apiErrorExample {json} Error-401:
     * { 
     *      msg: 'Restaurant does not exist' 
     * }
     * @apiError {statuscode} 400 unsuccessful
     * @apiError {statuscode} 401 restaurant does not exist
     */
    app.get('/restaurant/getFirst3CommentById/:id', restaurant.getFirst3CommentById);

	/**
     *  @apiVersion 2.2.0
	* @api {post} /restaurant/addComment Add comment
	* @apiGroup Restaurant
    * @apiParam {String} name the name of the commentator
    * @apiParam {Nummber} idRestaurant idRestaurant id of the restaurant want to comment on
    * @apiParam {String} content content of comment
	*
	* @apiSuccess {String} msg <code>"Have commented successfully"</code> if <code>statuscode = 200</code>, <code>"Restaurant does not exist"</code> if <code>statuscode = 401</code> and <code>"Params invalid"</code> if <code>statuscode = 402</code>
    * @apiSuccess {Object} result (if <code>statuscode = 200</code>) infomation of comments
    * @apiSuccessExample {json} Success-Response:
	* {
    *   "msg": "Have commented successfully",
    *   "result": {
    *       "id": 1,
    *       "idRestaurant": 1,
    *       "content": "Ngon quá",
    *       "name": "Trương Vô Kỵ",
    *       "createAt": "2018-12-18T15:29:29.829Z"
    *   }
    * }
	* * @apiSuccessExample {json} Example-Obj to send:
	* {
	*   "name": "Trương Vô Kỵ",
	*   "idRestaurant": "1",
	*   "content": "Ngon quá"
    * }
	* @apiError {statuscode} 400 other error
    * @apiError {statuscode} 401 Restaurant does not exist
    * @apiError {statuscode} 402 Params invalid
	* @apiErrorExample {json} Error-401:
	*     {
	*       "msg": "Restaurant does not exist"
    *     }
    * @apiErrorExample {json} Error-402:
	*     {
	*       "msg": "Params invalid"
	*     }
	*/
    app.post('/restaurant/addComment', restaurant.addComment);
};
