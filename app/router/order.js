const order = require('../controller/order');
const order_detail = require('../controller/order_detail');
module.exports = function (app, passport) {
    /**
   * @apiVersion 2.0.1
   * @api {post} /order/create create order
   * @apiGroup Order
   *  @apiDescription create order
   * @apiHeader {bearer} Token
	 *
     * 
	 * @apiParam {number} totalPrice
	 * @apiParam {string} Street
	 * @apiParam {number} idDistrict
	 * @apiParam {number} idWard
	 * @apiParam {string} phone
     * @apiParam {string} idRestaurant
     * @apiParam {Object[]} item 
	 * @apisuccess {statuscode} 200 successful
	 * @apiExample {json} Example usage:
	 *     {
	* "totalPrice":"100000",
	* "Street":"An dương vương",
	* "idWard":"9223",
	* "idDistrict":"13096",
    * "phone":"123456",
    * "idRestaurant": "1",
	* "item":[
	* 	{
	* 		"idFood":"1001",
	* 		"quantity":"1",
	* 		"note":"aaa"
	* 	},
	* 	{
	* 		"idFood":"1002",
	* 		"quantity":"1",
	* 		"note":"aaa"
	* 	}
	* ]
    * }
	*
	 * @apiError {statuscode} 400 unsuccessfull
     * @apiError {statuscode} 401 unauthorized
	 */
    app.post('/order/create', order.create);
     /**
   * @apiVersion 2.0.1
   * @api {post} /order/create/v2 create order version 2
   * @apiGroup Order
   *  @apiDescription create order
   * @apiHeader {bearer} Token
	 *
     * 
	 * @apiParam {number} totalPrice
	 * @apiParam {string} address
	 * @apiParam {string} phone
     * @apiParam {string} idRestaurant
     *  @apiParam {string} note
     * @apiParam {Object[]} item 
	 * @apisuccess {statuscode} 200 successful
	* @apiExample {json} Example usage:
	* {
	* "totalPrice":"100000",
	* "address": "An dương Vương",
	* "phone":"123456",
	* "idRestaurant": "1",
	* "item":[
	*	{
	*		"idFood":"1001",
	*		"quantity":"1",
	*		"note":"aaa"
	*	},
	*	{
	*		"idFood":"1002",
	*		"quantity":"1",
	*		"note":"aaa"
	*	}
	*]
* }
	*
	*
	 * @apiError {statuscode} 400 unsuccessfull
     * @apiError {statuscode} 401 unauthorized
	 */
    app.post('/order/create/v2',order.createv2)
     /**
   * @apiVersion 2.0.1
   * @api {get} /order/getAll get all history order of user
   * @apiGroup Order
   *  @apiDescription get all history order of user
   * @apiHeader {bearer} Token
	 * @apisuccess {statuscode} 200 successful
	 * @apisuccessExample {json} Success-Response:
	 *     {
	* [
    * {
    *     "id": 30,
    *     "idUser": 6,
    *     "totalPrice": 100000,
    *     "date": "2018-10-13T15:36:57.745Z",
    *     "address": "An dương vương, Phường Cầu Ông Lãnh, Quận 1",
    *     "idRestaurant": 1,
    *     "phone": "123456",
    *     "status": -1
    * },
    {..},
    * ]
	*
	 * @apiError {statuscode} 400 unsuccessfull
     *  @apiError {statuscode} 401 unauthorized
	 */
    app.get('/order/getAll', order.getAll);
     /**
   * @apiVersion 2.0.1
   * @api {get} /order/getOrder/:id get detail order by id
   * @apiGroup Order
   * @apiHeader {bearer} Token
	 * @apisuccess {statuscode} 200 successful
	 * @apisuccessExample {json} Success-Response:
	 * {
    * "order": {
    *     "id": 28,
    *     "idUser": 6,
    *     "totalPrice": 100000,
    *     "date": "2018-10-13T10:20:37.448Z",
    *     "address": "An dương vương, Phường Cầu Ông Lãnh, Quận 1",
    *     "idRestaurant": 1,
    *     "phone": "123456",
    *     "status": -1
    * },
    * "details": [
    *    {
    *        "id": 37,
    *        "idOrder": 28,
    *       "idFood": 1001,
    *       "quantity": 1,
    *      "note": "aaa"
    *  },
    *  {
    *     "id": 38,
    *     "idOrder": 28,
    *     "idFood": 1002,
    *     "quantity": 1,
    *     "note": "aaa"
    * }
    * ]
    * }
	*
	 * @apiError {statuscode} 400 unsuccessfull
     * @apiError {statuscode} 401 unauthorized
	 */
    app.get('/order/getOrder/:id', order.getOrderById);
    // app.get('/order/updateStatus/:idOrder/:status', order.updateStatus);
    // app.get('/order/completed/:idUser', passport.authenticate('jwt'), order.getOrderCompleted);

    // app.post('/orderDetail/create', order_detail.create)

    /**
   * @apiVersion 2.0.1
   * @api {get} /order/cancelOrder/:id cancel order if order not delivery
   * @apiGroup Order
   * @apiHeader {bearer} Token
	 * @apisuccess {statuscode} 200 successful
	 * @apisuccessExample {json} Success-Response:
	* {
    * "msg": "Successfull"
    * }
	*
	 * @apiError {statuscode} 400 unsuccessfull
     * @apiError {statuscode} 401 unauthorized
     * @apiError {statuscode} 402 Can't cancel order
	 */
    app.get('/order/cancelOrder/:id',order.cancelOrder) // Cancel order

};