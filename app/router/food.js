const food = require('../controller/food')
module.exports = function (app, passport) {
    app.get('/food/getAll', food.getAllFood); //ok
    app.put('/food/update', food.update);
    app.post('/food/create', food.create); //test

    /**
   * @apiVersion 2.0.1
	 * @api {get} /food/searchfood Search food by name
	 * @apiGroup Food
   * @apiParam {String} foodname name of food
	 *
	 * @apisuccess {statuscode} 200 successful
	 * @apisuccessExample {json} Success-Response:
	 *[
    *
    *    {
    *        "id": 1004,
    *        "name": "Trà dâu Nam Mỹ chanh vàng",
    *        "idFoodCategory": 2,
    *        "idRestaurant": 22,
    *        "price": 50000,
    *        "sold": 263,
    *        "image": "https://tea-3.lozi.vn/v1/ship/resized/heekcaa-by-royaltea-quan-hoan-kiem-ha-noi-6-1-1514629236?w=320&type=o"
    *    },
    *    {
    *        "id": 4002,
    *        "name": "Trà ô long mộc hoa kem cheese",
    *        "idFoodCategory": 2,
    *        "idRestaurant": 25,
    *        "price": 49000,
    *        "sold": 263,
    *        "image": "https://media.foody.vn/res/g71/707215/s570x570/2017123011210-2017127114251-7.-tra-olong-dao-kem-cheese.jpg"
    *    }
    *]
	 *
	 * @apiError {statuscode} 400 unsuccessful
	 */
    app.get('/food/searchfood', food.searchByName);

      /**
   * @apiVersion 2.0.1
	 * @api {get} /food/searchtype Search food by name of category
	 * @apiGroup Food
   * @apiParam {String} categoryname name of category
	 *
	 * @apisuccess {statuscode} 200 successful
	 * @apisuccessExample {json} Success-Response:
	 *[
    *
    *    {
    *        "id": 4005,
    *        "name": "Socola cheese",
    *        "idFoodCategory": 2,
    *        "idRestaurant": 22,
    *        "price": 53000,
    *        "sold": 385,
    *        "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbvPI9ZcVSBWKIe0ziwVqbfBYoVfBEljQYTwVKuh5sMd42Z9mWxg"
    *    },
    *    {
    *        "id": 4009,
    *        "name": "Trà dâu Nam Mỹ chanh vàng",
    *        "idFoodCategory": 2,
    *        "idRestaurant": 22,
    *        "price": 50000,
    *        "sold": 263,
    *        "image": "https://tea-3.lozi.vn/v1/ship/resized/heekcaa-by-royaltea-quan-hoan-kiem-ha-noi-6-1-1514629236?w=320&type=o"
    *    }
    *]
	 *
	 * @apiError {statuscode} 400 unsuccessful
	 */
    app.get('/food/searchtype', food.searchByCategory);
     /**
   * @apiVersion 2.0.1
	 * @api {get} /food/:id Get food by id
	 * @apiGroup Food
	 *
	 * @apisuccess {statuscode} 200 successful
	 * @apisuccessExample {json} Success-Response:
	* {
    * "food": {
    *    "id": 1301,
    *    "name": "Combo \"Khai trương tưng bừng\" giá 449k (2 - 3 người)",
    *    "idFoodCategory": 6,
    *    "idRestaurant": 13,
    *    "image": "https://media.foody.vn/res/g12/113651/s600x600/201891482210-bua-ngon-du-day.jpg",
    *    "price": 289000,
    *    "sold": 56
    *}
    *}
	
	 */
    app.get('/food/:id', food.getFood);
};