const notification = require('../controller/notification')
module.exports = function (app, passport) {
    /**
   * @apiVersion 2.0.3
   * @api {get} /getNoti Get noti for user
   * @apiGroup Notification
   * @apiHeader {bearer} Token
   * @apiHeader {Content-Type} Key
   * @apiHeader {application/json} Value
   *
   * @apisuccess {statuscode} 200 successful
   * @apisuccessExample {json} Success-Response:
   *[
   *	{
   *     "id": 1,
   *     "idUser": 10,
   *     "title": "The Alley giảm giá 50%",
   *     "image": null,
   *     "content": "Nhân ngày 20/11 giảm giá 50% tất cả các chi nhánh của The Alley"
   *  },
   *	{
   *     "id": 2,
   *     "idUser": 10,
   *     "title": "GongCha tri ân thầy cô",
   *     "image": null,
   *     "content": "Nhân ngày 20/11 Gong cha giảm giá 70% toàn bộ menu"
   *  }
   *]
   *
   * @apiError {statuscode} 401 unauthorized
   * @apiErrorExample {json} Error-Response:
   *     {
   *       "msg": "unauthorized"
   *     }
   */
  app.get('/getNoti', notification.getNotifications);
  /**
   * @apiVersion 2.0.3
   * @api {post}/deviceToken Post device token 
   * @apiGroup Notification
   * @apiHeader {bearer} Token
   * @apiHeader {Content-Type} Key
   * @apiHeader {application/json} Value
   * @apiParam {string} deviceId
	 * @apiParam {string} platform (android/ios)
   * @apisuccess {statuscode} 200 successful
   * @apisuccessExample {json} Success-Response:
   *	{
   *     "id": 1,
   *     "idUser": 10,
   *     "deviceId": "jahjakj",
   *     "platform": android,
   *  }
   *
   * @apiError {statuscode} 401 unauthorized
   * @apiErrorExample {json} Error-Response:
   *     {
   *       "msg": "unauthorized"
   *     }
   *  @apiError {statuscode} 400 other error
   */
  app.post('/deviceToken',notification.registerNotification)
  /**
   * @apiVersion 2.0.3
   * @api {post}/pushNotificationtoUser Send notification to user
   * @apiGroup Notification
   * @apiHeader {bearer} Token
   * @apiHeader {Content-Type} Key
   * @apiHeader {application/json} Value
   * @apiParam {string} title
	 * @apiParam {string} content
   * @apisuccess {statuscode} 200 successful
   * @apiError {statuscode} 400 failure
  **/
  app.post('/pushNotificationtoUser',notification.pushNotificationtoUser)
  /**
   * @apiVersion 2.0.3
   * @api {post}/pushNotificationtoAll Send notification to all user
   * @apiGroup Notification
   * @apiHeader {Content-Type} Key
   * @apiHeader {application/json} Value
   * @apiParam {string} title
	 * @apiParam {string} content
   * @apisuccess {statuscode} 200 successful
   * @apiError {statuscode} 400 failure
  **/
  app.post('/pushNotificationtoAll',notification.pushNotificationtoAll)
}