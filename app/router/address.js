const district = require('../controller/district');
const ward= require('../controller/ward');
const address = require('../controller/address');
module.exports = function (app) {
  /**
   * @apiVersion 2.0.1
   * @api {get} /district/getAll get all district
   * @apiGroup Address
   *  @apiDescription Get all district in HCM city
   * @apiSuccess {statuscode} 200 successful
   *
   */
    app.get('/district/getAll',district.getAll);
  /**
   * @apiVersion 2.0.1
   * @api {get} /ward/getAllByDistrict get ward by district
   * @apiGroup Address
   * @apiDescription Get all wards by district
   * @apiParam {int} id The id of district
   * @apiSuccess {statuscode} 200 successful
   *
   */
    app.get('/ward/getAllByDistrict',ward.getAll);
    app.get('/address/getByid/:id',address.getById);
    app.get('/a',address.getAll)
};