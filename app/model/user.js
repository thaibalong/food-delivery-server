'use strict';
module.exports = (sequelize, DataTypes) => {
    var user = sequelize.define('USERS', {
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
              }
        },
        password: {
            type: DataTypes.STRING
        },
        phone: {
            type: DataTypes.STRING
        },
        idAdress: {
            type: DataTypes.INTEGER,
            references:{
                model: "ADDRESSES",
                key:"id"
            }
        },
        isActive: {
            type: DataTypes.BOOLEAN
        },
        userName: {
          type: DataTypes.STRING,
        },
        avatar: {
            type: DataTypes.STRING,
        }

    }, {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            tableName: 'USERS',
            timestamps: false
        });
    return user;
};