'use strict';
module.exports = (sequelize, DataTypes) => {
    var ward = sequelize.define('DISTRCTS', {
        name: {
            type: DataTypes.STRING,
        },
    }, {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            tableName: 'DISTRICTS',
            timestamps: false
        });
    return ward;
}