'use strict';
module.exports = (sequelize, DataTypes) => {
  var notifications = sequelize.define('NOTIFICATIONS', {
    idUser: {
      type: DataTypes.INTEGER,
      references:{
        model: "USERS",
        key:"id"
      }
    },
    title: {
      type: DataTypes.STRING
    },
    image: {
      type: DataTypes.STRING
    },
    content: {
      type: DataTypes.STRING
    },
  }, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci',
    tableName: 'NOTIFICATIONS',
    timestamps: false
  });
  return notifications;
};