'use strict';
module.exports = (sequelize, DataTypes) => {
    var order_detail = sequelize.define('ORDER_DETAILS', {
        idOrder:{
            type: DataTypes.INTEGER,
            references:{
                model: "ORDERS",
                key:"id"
            }
        },
        idRestaurant: {
            type: DataTypes.INTEGER,
            references:{
                model: "RESTAURANTS",
                key:"id"
            }
        },
        idFood: {
            type: DataTypes.INTEGER,
            references:{
                model: "FOOD",
                key:"id"
            }
        },
        quantity: {
            type: DataTypes.INTEGER,
        },
        note: {
            type: DataTypes.TEXT,
        },
    }, {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            tableName: 'ORDERS_DETAILS',
            timestamps: false
        });
    return order_detail;
}