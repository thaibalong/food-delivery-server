'use strict';
module.exports = (sequelize, DataTypes) => {
  var comments = sequelize.define('COMMENTS', {
    idRestaurant: {
        type: DataTypes.INTEGER,
        references:{
            model: "RESTAURANTS",
            key:"id"
        }
    },
    name: {
      type: DataTypes.STRING
    },
    content: {
      type: DataTypes.STRING
    },
    createAt:{
        type: DataTypes.TIME
    },
  }, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci',
    tableName: 'COMMENTS',
    timestamps: false
  });
  return comments;
};