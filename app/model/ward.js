'use strict';
module.exports = (sequelize, DataTypes) => {
    var ward = sequelize.define('WARDS', {
        name: {
            type: DataTypes.STRING,
        },
        idDistrict: {
            type: DataTypes.INTEGER,
            references:{
                model: "DISTRICTS",
                key:"id"
            }
        },
    }, {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            tableName: 'WARDS',
            timestamps: false
        });
    return ward;
}