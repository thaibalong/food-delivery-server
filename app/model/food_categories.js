'use strict';
module.exports = (sequelize, DataTypes) => {
    var categories = sequelize.define('FOOD_CATEGORIES', {
        name: {
            type: DataTypes.STRING,
        },
        image: {
            type: DataTypes.STRING,
        },
    }, {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            tableName: 'FOOD_CATEGORIES',
            timestamps: false
        });
    return categories;
};