'use strict';
module.exports = (sequelize, DataTypes) => {
    var restaurant = sequelize.define('RESTAURANTS', {
        name: {
            type: DataTypes.STRING
        },
        idAddress: {
            type: DataTypes.INTEGER,
            references:{
                model: "ADDRESSES",
                key:"id"
            }
        },
        rating: {
            type: DataTypes.INTEGER
        },
        image: {
            type: DataTypes.STRING
        },
        timeOpen:{
            type: DataTypes.TIME
        },
        timeClose:{
            type: DataTypes.TIME
        },
        feeShip: {
            type: DataTypes.INTEGER,
        }
    }, {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            tableName: 'RESTAURANTS',
            timestamps: false
        });
    return restaurant;
};