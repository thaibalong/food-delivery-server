'use strict';
module.exports = (sequelize, DataTypes) => {
    var address = sequelize.define('ADDRESSES', {
        idDistrict: {
            type: DataTypes.INTEGER,
            references:{
                model: "DISTRICTS",
                key:"id"
            }
        },
        idWard: {
            type: DataTypes.INTEGER,
            references:{
                model: "WARDS",
                key:"id"
            }
        },
        street: {
            type: DataTypes.STRING
        },
        latitude: {
            type: DataTypes.DOUBLE
        },
        longitude: {
            type: DataTypes.DOUBLE
        }
    }, {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            tableName: 'ADDRESSES',
            timestamps: false
        });
    return address;
};