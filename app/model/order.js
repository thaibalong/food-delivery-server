'use strict';
module.exports = (sequelize, DataTypes) => {
    var order = sequelize.define('ORDERS', {
        idUser:{
            type: DataTypes.INTEGER,
            references:{
                model: "USERS",
                key:"id"
            }
        },
        totalPrice: {
            type: DataTypes.INTEGER
        },
        date:{
            type:DataTypes.DATE
        },
        address:{
            type: DataTypes.STRING
        },
        phone:{
            type: DataTypes.STRING
        },
        idRestaurant: {
            type: DataTypes.INTEGER,
            references:{
                model: "RESTAURANTS",
                key:"id"
            }
        },
        status:{
            type: DataTypes.INTEGER // 0: not 1: proprocessing 2/ completed -1/ cancel order  -2 /rejected
        },
        note: {
            type: DataTypes.STRING
        }
    }, {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            tableName: 'ORDERS',
            timestamps: false
        });
    return order;
}