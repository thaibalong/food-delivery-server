'use strict';
module.exports = (sequelize, DataTypes) => {
  var device_token = sequelize.define('DEVICE_TOKEN', {
    idUser: {
      type: DataTypes.INTEGER,
      references:{
        model: "USERS",
        key:"id"
      }
    },
    deviceId: {
      type: DataTypes.STRING
    },
    platform: {
      type: DataTypes.STRING
    },
  }, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci',
    tableName: 'DEVICE_TOKEN',
    timestamps: false
  });
  return device_token;
};