'use strict';
module.exports = (sequelize, DataTypes) => {
    var food = sequelize.define('FOOD', {
        name: {
            type: DataTypes.STRING,
        },
        idFoodCategory: {
            type: DataTypes.INTEGER,
            references:{
                model: "FOOD_CATEGORIES",
                key:"id"
            }
        },
        idRestaurant: {
            type: DataTypes.INTEGER,
            references:{
                model: "RESTAURANTS",
                key:"id"
            }
        },
        image: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.INTEGER
        },
        sold: {
            type: DataTypes.INTEGER
        }
    }, {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            tableName: 'FOOD',
            timestamps: false
        });
    return food;
};