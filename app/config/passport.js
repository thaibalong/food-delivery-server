var LocalStrategy = require('passport-local').Strategy;
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt-nodejs')
const db = require('../config/db.config.js');
const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const user = db.user;
module.exports = (passport)=>{
    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    
    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        session: false,
      },
        function (email, password, done) {
          user.findOne({where : {
                email:email,
            }
          }).then(function (user) {
            const token = jwt.sign({data:`${user.email}`}, 'secret', { expiresIn: 60 * 60 *24 *30 });
            if (!user || !bcrypt.compareSync(password,user.password)) {
              return done(null, false, { message: 'Error' });
            }
            return done(null, token);
          }).catch(err=>{
            return done(null, false, { message: 'Error' });
          });
        }
      ));   
      passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : 'secret'
      },
      function (jwtPayload, cb) {
        user.findOne({where : {
          email:jwtPayload.data,
        }}).then(user => {
                return cb(null, user);
            })
            .catch(err => {
                return cb(err);
            });
      }
      )); 
}
   