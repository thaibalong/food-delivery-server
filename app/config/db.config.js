const settings = require('../../setting');
var Sequelize = require("sequelize");
var config = settings.databaseConfig;
var sequelize = new Sequelize(config);

sequelize.authenticate()
  .then(() => console.log('connect database success!!'))
  .catch(err => console.log(err.message));

var db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require('../model/user.js')(sequelize, Sequelize);
db.restaurant = require('../model/restaurant')(sequelize, Sequelize);
db.order = require('../model/order.js')(sequelize, Sequelize);
db.ward = require('../model/ward')(sequelize, Sequelize);
db.order_detail = require('../model/order_detail')(sequelize, Sequelize);
db.food_categories = require('../model/food_categories')(sequelize, Sequelize);
db.food = require('../model/food')(sequelize, Sequelize);
db.district = require('../model/district')(sequelize, Sequelize);
db.address = require('../model/Address')(sequelize, Sequelize);
db.notifications = require('../model/notifications')(sequelize, Sequelize);
db.device_token = require('../model/device_token')(sequelize, Sequelize);
db.comments = require('../model/comments')(sequelize, Sequelize);

// //define table associations

// //HasOne associations
db.address.hasOne(db.restaurant, { foreignKey: 'idAddress' }) //`idAddress` will be added on restaurant / Target model
// db.address.hasOne(db.user, { foreignKey: 'idAddress' }) //`idAddress` will be added on user / Target model
// db.ward.hasOne(db.address, { foreignKey: 'idWard' }) //`idWard` will be added on address / Target model
// db.district.hasOne(db.address, { foreignKey: 'idDistrict' }) //`idDistrict` will be added on address / Target model
// association of table food and restaurant
db.order_detail.belongsTo(db.order, { foreignKey: 'idOrder' });
db.order.hasMany(db.order_detail, { foreignKey: 'idOrder' });
db.food.belongsTo(db.restaurant, { foreignKey: 'idRestaurant' });
db.restaurant.hasMany(db.food, { foreignKey: 'idRestaurant' });


module.exports = db;

