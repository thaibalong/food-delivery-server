const db = require('../config/db.config.js');
const food_categories = db.food_categories;

exports.create =(req, res) => {
	food_categories.create(req.body).then(food_categories => {
        res.status(200).json(food_categories)
        }).catch(err => {
            res.status(400).json({ msg: err});
        });
};
exports.update = (req, res, next) => {
    id = req.params.id;
	food_categories .update(req.body,{
        where:{
            id:id
        }
    }).then(food_categories=>{
        res.status(200).json(food_categories)
    }).catch(err=>{
        res.status(400).json({msg:err})
    })
};

exports.getAllOrder = (req, res) => {
    food_categories.findAll()
      .then((food_categories) => { res.status(200).send(food_categories)})
};

exports.getOderById = (req, res) => {
    let id = req.params.id;
    food_categories.findById(id).then((food_categories)=> {
        res.status(200).json(food_categories)
    }).catch((err) => {
        res.status(400).json( {
            msg: err
        })
    })
	
};

exports.getAllCategories = (req, res) => {
  food_categories.findAll()
    .then((food_categories) => { res.status(200).send(food_categories) })
};

exports.delete=(req,res)=>{
	order.destroy({where: {},
		truncate: true}).then(()=>{res.send()})
};

