const db = require('../config/db.config.js');
const address = db.address;
const district = db.district;
const ward = db.ward;

exports.create =(req, res) => {
	address.create(req.body).then(address => {
        res.status(200).json(address)
        }).catch(err => {
            res.status(400).json({ msg: err})
        })
}

exports.getAll = (req, res) => {
	address.findAll().then((address) => { res.status(200).send(address) })
}

exports.getById = async (id) => {
    if (id === null) return null;
    let add,dis,wa
    try {
        add = await address.findById(id)
        dis = await district.findById(add.idDistrict)
        wa =  await ward.findById(add.idWard)
        var result = {
            address: `${add.street}, ${wa.name},${dis.name}`,
            latitude: add.latitude,
            longitude: add.longitude
        }
        return result
    }catch(err){
        return null
    }
};
exports.delete=(req,res)=>{
	address.destroy({where: {},
		truncate: true}).then(()=>{res.send()})
}

exports.updateOrCreate = async (add,id) =>{
    let found, create, up;
    try{
        found = await address.findOne({where: {id:id}})
        if (!found) {
            create= await address.create(add)
            return create
        }
        else {
            up = await address.update(add,{where:{id:id}})
            return up
        }
    }catch(err){
        return err
    }
}