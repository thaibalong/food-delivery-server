const db = require('../config/db.config.js');
const district = db.district;

exports.create =(req, res) => {
	district.create(req.body).then(district => {
        res.status(200).json(district)
        }).catch(err => {
            res.status(400).json({ msg: err})
        })
}
exports.update = (req, res, next) => {
    id = req.params.id
	district.update(req.body,{
        where:{
            id:id
        }
    }).then(district=>{
        res.status(200).json(district)
    }).catch(err=>{
        res.status(400).json({msg:err})
    })
};

exports.getAll = (req, res) => {
	district.findAll().then((district) => { res.status(200).send(district) })
};

exports.getById = (id, res) => {
    district.findById(id).then((district)=> {
        res.status(200).json(district)
    }).catch((err) => {
        res.status(400).json({
            msg: err
        })
    })
	
};
exports.delete=(req,res)=>{
	district.destroy({where: {},
		truncate: true}).then(()=>{res.send()})
}

