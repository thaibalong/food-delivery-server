const db = require('../config/db.config.js');
const ward = db.ward;

exports.create =(req, res) => {
	ward.create(req.body).then(ward => {
        res.status(200).json(ward)
        }).catch(err => {
            res.status(400).json({ msg: err});
        });
};
exports.update = (req, res, next) => {
    id = req.params.id
	ward.update(req.body,{
        where:{
            id:id
        }
    }).then(ward=>{
        res.status(200).json(ward)
    }).catch(err=>{
        res.status(400).json({msg:err})
    })
};

exports.getAll = (req, res) => {
	ward.findAll({
        where :{
            idDistrict: req.query.id
        }
    }).then((ward) => { res.status(200).send(ward) })
};

exports.getById = (id,res) => {
    ward.findById(id).then((ward)=> {
        res.status(200).json(ward)
    }).catch((err) => {
        res.status(400).json({
            msg: err
        })
    })
	
};
exports.delete=(req,res)=>{
	ward.destroy({where: {},
		truncate: true}).then(()=>{res.send()})
}

