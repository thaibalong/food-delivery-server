const db = require('../config/db.config.js');
const user = db.user;
var validator = require("email-validator");
var fs = require('fs');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
var passport = require('passport');
var smtpTransport = require('../send_email');
var address = require('../controller/address');

sendVerificationEmail = (verificationMail, req, res) => {
	link = "http://" + req.get('host') + "/verify?sign=" + verificationMail.id;
	mailOptions = {
		from: 'food.delivery.server.cq2015.31@gmail.com',
		to: verificationMail.email,
		subject: "Please confirm your Email account",
		html: "Hello,<br> Please Click on the link to verify your email.<br><a href=" + link + ">Click here to verify</a>"
	};
	smtpTransport.sendMail(mailOptions, function (error, response) {
		if (error) {
			console.log(error);
			res.end("error");
		} else {
			console.log("Message sent: " + response.message);
			res.end("sent");
		}
	});
};

sendNewPasswordEmail = (req, res) => {
	mailOptions = {
		from: 'food.delivery.server.cq2015.31@gmail.com',
		to: req.email,
		subject: "Reset delivery food password",
		html: "Hello,<br> Your new password is: <br><h>" + req.newPassword.toString() + "</h>"
	};
	smtpTransport.sendMail(mailOptions, function (error, response) {
		if (error) {
			console.log(error);
			res.end("error");
		} else {
			console.log("Message sent: " + response.message);
			res.end("sent");
		}
	});
};

exports.create = (req, res) => {
	if (validator.validate(req.body.email)) {
		req.body.password = bcrypt.hashSync(req.body.password, null, null);
		user.findAll({
			where: {
				email: req.body.email
			}
		}).then((checkUser) => {
			if (checkUser.length === 0) {
				var newUser = {
					email: req.body.email,
					password: req.body.password,
					isActive: false
				};
				user.create(newUser).then(user => {
					const token = jwt.sign({ data: `${user.email}` }, 'secret1', { expiresIn: 60 * 3 });
					res.status(200).json({ id: user.id });
					var verificationMail = {
						email: user.email,
						id: token
					};
					sendVerificationEmail(verificationMail, req, res);
				}).catch(err => {
					console.log(err);
					res.status(500).json({ msg: err });
				});
			}
			else {
				res.status(400).json({ msg: "Email already exists" });
			}
		})
	}
	else {
		res.status(401).json({ msg: "Invalid email" });
	}
};
exports.createv2 = (req, res) => {
	if (validator.validate(req.body.email)) {
		req.body.password = bcrypt.hashSync(req.body.password, null, null);
		user.findAll({
			where: {
				email: req.body.email
			}
		}).then((checkUser) => {
			if (checkUser.length === 0) {
				var newUser = {
					email: req.body.email,
					password: req.body.password,
					phone: req.body.phone,
					userName: req.body.userName,
					isActive: false
				};
				user.create(newUser).then(user => {
					const token = jwt.sign({ data: `${user.email}` }, 'secret1', { expiresIn: 60 * 3 });
					res.status(200).json({ id: user.id });
					var verificationMail = {
						email: user.email,
						id: token
					};
					sendVerificationEmail(verificationMail, req, res);
				}).catch(err => {
					console.log(err);
					res.status(500).json({ msg: err });
				});
			}
			else {
				res.status(400).json({ msg: "Email already exists" });
			}
		})
	}
	else {
		res.status(401).json({ msg: "Invalid email" });
	}
};
exports.findById = async (req, res, next) => {
	passport.authenticate('jwt', async (err, user) => {
		if (err || !user) {
			return res.status(401).json({
				msg: 'unauthorized',
			});
		}
		let	add = await db.address.findById(user.idAdress)
		// let add = await address.getById(user.idAdress);
		var AvtUrl = req.headers.host + '/' + user.id + '.jpg';
		if (user.avatar === null) AvtUrl = req.headers.host + '/avatar-default.jpg';
		return res.status(200).json({
			email: user.email,
			phone: user.phone,
			address: add,
			userName: user.userName,
			avatarUrl: AvtUrl,
		});
	})(req, res, next);
};
exports.findById2 = async (req, res, next) => {
	passport.authenticate('jwt', async (err, user) => {
		if (err || !user) {
			return res.status(401).json({
				msg: 'unauthorized',
			});
		}
		let add = await address.getById(user.idAdress);
		// var AvtUrl = req.headers.host + '/' + user.id + '.jpg';
		if (user.avatar === null) user.avatar = 'http://' + req.headers.host + '/avatar-default.jpg';
		return res.status(200).json({
			email: user.email,
			phone: user.phone,
			address: add,
			userName: user.userName,
			avatarUrl: user.avatar,
		});
	})(req, res, next);
};
exports.updatePassword = async (req, res, next) => {
	try {
		passport.authenticate('jwt', async (err, users) => {
			if (err || !users) {
				return res.status(401).json({
					msg: 'unauthorized',
				});
			}
			var newpassword = bcrypt.hashSync(req.body.password, null, null);
			var newuser = {
				password: newpassword
			};
			await user.update(newuser, { where: { id: users.id } });
			return res.status(200).json({ msg: "Update succesfull" })
		})(req, res, next);
	} catch (err) {
		return res.status(400).json({ msg: err })
	}
};

exports.updateInfo = async (req, res, next) => {
	try {
		passport.authenticate('jwt', async (err, users) => {
			if (err || !users) {
				return res.status(401).json({
					msg: 'unauthorized',
				});
			}
			let newaddress = {
				idDistrict: req.body.idDistrict,
				idWard: req.body.idWard,
				street: req.body.street
			};
			let newadd = await address.updateOrCreate(newaddress, users.idAdress);
			var newuser = {
				phone: req.body.phone,
				idAdress: newadd.id,
				userName: req.body.userName,
			};
			await user.update(newuser, { where: { id: users.id } });
			return res.status(200).json({ msg: "Update succesfull" })
		})(req, res, next);
	} catch (err) {
		return res.status(400).json({ msg: err })
	}
};
exports.verifyUser = async (req, res) => {
	try {
		const ids = jwt.verify(req.query.sign, 'secret1');
		var email = ids.data;
		await user.update({ isActive: true }, { where: { email: email } });
	} catch (err) {
		return res.status(400).json({ msg: err });
	}
	return res.status(200).json('successful');
};
exports.forgetPassword = (req, res) => {
	const password = Math.floor((Math.random() * 1000000) + 10000);
	console.log(password);
	const hashPassword = bcrypt.hashSync(password, null, null).toString();
	const newPassword = {
		password: hashPassword
	};
	user.findOne({
		where: {
			email: req.body.email
		}
	}).then((userFound) => {
		userFound.update(newPassword, {
			where: {
				email: req.body.email
			}
		}).then(() => {
			var email = {
				email: req.body.email,
				newPassword: password
			};
			sendNewPasswordEmail(email, res);
			res.status(200).json({ msg: 'Reset password successfully, an email has been sent to:  -> user email = ' + req.body.email });
		}).catch(err => {
			console.log(err);
			res.status(500).json({ msg: err });
		});
	}).catch(err => {
		res.status(400).json({ msg: "email is wrong" });
	});

};

exports.login = (req, res, next) => {
	let users;
	user.findOne({
		where: {
			email: req.body.email
		}
	}).then((found) => {
		users = found;
		if (!found.isActive) {
			return res.status(401).json({ msg: "Account not verified " });
		}
	}).catch((err) => {
		res.status(400).json({ msg: "Email or Password is wrong" });
	});
	passport.authenticate('local', (err, token) => {
		if (err || !token) {
			return res.status(400).json({
				msg: 'Email or Password is wrong',
			});
		}
		var AvtUrl = req.headers.host + '/' + users.id + '.jpg';
		if (users.avatar === null) AvtUrl = req.headers.host + '/avatar-default.jpg';
		return res.status(200).json({
			msg: "Login successful",
			token: token,
			id: users.id,
			email: users.email,
			phone: users.phone,
			avatarUrl: AvtUrl,
		});
	})(req, res, next);
};
exports.loginv2 = (req, res, next) => {
	let users;
	user.findOne({
		where: {
			email: req.body.email
		}
	}).then((found) => {
		users = found;
		if (!found.isActive) {
			return res.status(401).json({ msg: "Account not verified " });
		}
	}).catch((err) => {
		res.status(400).json({ msg: "Email or Password is wrong" });
	});
	passport.authenticate('local', (err, token) => {
		if (err || !token) {
			return res.status(400).json({
				msg: 'Email or Password is wrong',
			});
		}
		var AvtUrl = users.avatar
		if (users.avatar === null) AvtUrl = 'http://' +req.headers.host + '/avatar-default.jpg';
		return res.status(200).json({
			msg: "Login successful",
			token: token,
			id: users.id,
			email: users.email,
			phone: users.phone,
			userName: users.userName,
			avatarUrl: AvtUrl,
		});
	})(req, res, next);
};
exports.findAllUser = (req, res) => {
	user.findAll().then((user) => { res.send(user) })
};
exports.delete = (req, res) => {
	var id = req.params.id
	user.destroy({ where: { id: id } }).then(() => { res.send() })
};
exports.verify = (req, res) => {
	try {
		const token = jwt.sign({ data: `${req.body.email}` }, 'secret1', { expiresIn: 60 * 60 * 3 });
		var verificationMail = {
			email: req.body.email,
			id: token
		};
		user.findOne({ where: { email: req.body.email, isActive: false } }).then((us) => {
			if (us != null) {
				sendVerificationEmail(verificationMail, req, res)
				return res.status(200).json({ msg: "Successfull" })
			}
			else
				return res.status(400).json({ msg: "Email verified" })
		})
	} catch (err) {
		return res.status(500).json({ msg: err })
	}
};
exports.updateAvatar = async (req, res, next) => {
	try {
		passport.authenticate('jwt', async (err, users) => {
			if (err || !users) {
				return res.status(401).json({
					msg: 'unauthorized',
				});
			}
			else {
				if (typeof req.file !== 'undefined') {
					fs.writeFile('public/' + users.id + '.jpg', req.file.buffer, (err) => {
						// throws an error, you could also catch it here
						if (err) {
							return res.status(400).json({ msg: err })
						};
						// success case, the file was saved
						user.update({ avatar: users.id }, { where: { id: users.id } })
						return res.status(200).json({ avatarUrl: req.headers.host + '/' + users.id + '.jpg' })
					});
				}
				else
					return res.status(400).json({ msg: err })
			}
		})(req, res, next);
	} catch (err) {
		return res.status(400).json({ msg: err })
	}
};
exports.updateAvatar2 = async (req, res, next) => {
	try {
		passport.authenticate('jwt', async (err, users) => {
			if (err || !users) {
				return res.status(401).json({
					msg: 'unauthorized',
				});
			}
			else {
				let old = await user.findById(users.id)
				try {
					fs.unlinkSync(`public/${old.avatar.split('/').pop()}`)
				}catch(e) {
					console.log(e)
				}  
				if (typeof req.file !== 'undefined') {
					let time=Date.now()
					fs.writeFile('public/' + users.id + 'T'+ time + '.jpg', req.file.buffer, (err) => {
						// throws an error, you could also catch it here
						if (err) {
							return res.status(400).json({ msg: err })
						};
						let file = 'http://' + req.headers.host + '/' + users.id  +'T'+ time + '.jpg'
						// success case, the file was saved
						user.update({ avatar:  file }, { where: { id: users.id } })
						return res.status(200).json({ avatarUrl:file })
					});
				}
				else
					return res.status(400).json({ msg: err })
			}
		})(req, res, next);
	} catch (err) {
		return res.status(400).json({ msg: err })
	}
};
exports.updateAvatarTest = async (req, res, next) => {
	try {

		// var newpassword = bcrypt.hashSync(req.body.password, null, null);
		// var newuser ={
		// 	password: newpassword
		// };
		// await user.update(newuser,{where:{id:users.id}});
		// return res.status(200).json({msg:"Update succesfull"})
		let formData = req.body;
		console.log(formData);
		console.log(req.file);
		console.log(req.headers.host)
		if (typeof req.file !== 'undefined') {
			fs.writeFile('public/2.jpg', req.file.buffer, (err) => {
				// throws an error, you could also catch it here
				if (err) {
					return res.status(400).json({ msg: err })
				};
				user.update({ avatar: 175 }, { where: { id: 175 } })
				const avatarUrl = req.headers.host + '/175.jpg';
				// success case, the file was saved
				//console.log('Lyric saved!');
				return res.status(200).json({ avatarUrl: avatarUrl })
			});
		}
		else
			return res.status(400).json({ msg: err })
		// fs.readFile(req.file.path,(err, contents)=> {
		// 	if (err) {
		// 	console.log('Error: ', err);
		//    }else{
		// 	console.log('File contents ',contents);
		//    }
		//   });

		// return res.status(200).json({ data: req.body })
	} catch (err) {
		return res.status(400).json({ msg: err })
	}
};
