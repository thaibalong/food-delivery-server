const db = require('../config/db.config.js');
const order = db.order_detail;

exports.create =(req, res) => {
	order.create(req.body).then(order => {
        res.status(200).json(order)
        }).catch(err => {
            res.status(400).json({ msg: err});
        });
};
exports.update = (req, res, next) => {
    id = req.params.id
	order.update(req.body,{
        where:{
            id:id
        }
    }).then(order=>{
        res.status(200).json(order)
    }).catch(err=>{
        res.status(400).json({msg:err})
    })
};

exports.getAll = (req, res) => {
	order.findAll().then((order) => { res.status(200).send(order) })
};

exports.getfoodById = (req, res) => {
    let id = req.params.id
    order.findById(id).then((order)=> {
        res.status(200).json(order)
    }).catch((err) => {
        res.status(400).json( {
            msg: err
        })
    })
	
};
exports.delete=(req,res)=>{
	order.destroy({where: {},
		truncate: true}).then(()=>{res.send()})
}

