const db = require('../config/db.config.js');
const restaurant = db.restaurant;
const food = db.food;
const address = require('../controller/address');
const db_Address = db.address;
const db_comments = db.comments;
const resource = [
    {
        url: 'https://thumbor.mumu.agency/unsafe/1000x562/https://www.theransomnote.com/media/articles/rare-african-music-tops-trendbases-restaurant-background-music-charts/4ca464fe-54ae-457f-8680-702aaa8a13ab.jpg',
        type: 'image'
    },
    {
        url: 'https://www.youtube.com/embed/AK8nlF6phjY',
        type: 'video'
    },
    {
        url: 'https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1363450/910/607/m1/fpnw/wm0/img_4185-.jpg?1465877099&s=d4891a812f1982d1db02d86dd507fe43',
        type: 'image'
    },
    {
        url: 'https://image.freepik.com/free-photo/background-blurry-restaurant-shop-interior_1203-4031.jpg',
        type: 'image'
    },
    {
        url: 'https://image.freepik.com/free-photo/wooden-table-with-blurred-of-the-restaurant-background_4043-229.jpg',
        type: 'image'
    },
    {
        url: 'https://www.youtube.com/embed/sTjBt4LsvoE',
        type: 'video'
    },
]


exports.create = (req, res) => {
    restaurant.create(req.body).then(restaurant => {
        res.status(200).json(restaurant);
    }).catch(err => {
        res.status(400).json({ msg: err });
    });
};
exports.getAllMerchants = (req, res, next) => {
    if (isNaN(req.params.per_page) || isNaN(req.params.page)) {
        res.status(401).json({ msg: "params is invalid" });
    }
    else {
        const query = {
            limit: req.params.per_page,
            offset: (req.params.page - 1) * req.params.per_page
        };
        restaurant.findAndCountAll(query).then(async (restaurant) => {
            //Nếu số lượng record nhỏ hơn per_page  ==> không còn dữ liệu nữa => trả về -1 
            var next_page = parseInt(req.params.page) + 1;
            //Kiểm tra còn dữ liệu không
            if ((parseInt(restaurant.rows.length) + (next_page - 2) * parseInt(req.params.per_page)) === parseInt(restaurant.count)) next_page = -1;
            var data = [];
            restaurant.rows.map(async (element, i) => {
                adr = address.getById(element.idAddress);
                data.push({
                    info: element,
                    address: adr
                })
            })
            return res.status(200).send(
                {
                    itemCount: restaurant.rows.length, //số lượng record được trả về
                    data: data, //mảng record
                    next_page: next_page //trang kế tiếp, nếu là -1 thì hết data rồi
                })
        }).catch(err => {
            res.status(400).json({ msg: err });
        });
    } (req, res, next)
};
exports.getMerchant = async (req, res, next) => {
    let id = req.params.id;
    let restaurantResult, foods, addressResult;
    try {
        restaurantResult = await restaurant.findById(id);
        addressResult = await address.getById(restaurantResult.idAddress);
        foods = await food.findAll({ where: { idRestaurant: id } });
    } catch (err) {
        return res.status(400).json({ msg: err })
    }
    return res.status(200).json({
        resource: resource,
        restaurant: restaurantResult,
        address: addressResult,
        menu: foods
    })(req, res, next)
};
exports.delete = (req, res) => {
    restaurant.destroy({
        where: {},
        truncate: true
    }).then(() => { res.send() })
};
//xong nhưng cần sửa chữa
//search bằng câu lệnh nó có phân biệt hoa thường nên tìm rất chuối
exports.search = (req, res) => {
    const name = '%' + req.query.name + '%';
    // var query = {
    //     where: {
    //         name: {
    //             $like: '%' + req.params.name + '%'
    //         },
    //     }
    // }
    restaurant.sequelize.query("select * from \"RESTAURANTS\" where name like '" + name + "' ").then(restaurant => {
        res.status(200).send(restaurant[0])
    }).catch(err => {
        res.status(400).json({ msg: err })
    })
};

exports.getRestaurantByCategory = async (req, res) => {
    const idCategory = req.params.idCategory;
    var query = {
        include: [{
            model: db.food,
            where: {
                idFoodCategory: idCategory,
            },
            attributes: []
        }],
        // attributes:['id','idAddress',[db.sequelize.fn(address.getById, db.sequelize.col('idAddress')), 'address']]
    }
    restaurant.findAll(query).then((restaurants) => {
        res.status(200).send(restaurants)
    }).catch((err) => {
        res.status(400).json({ msg: err })
    })
};

function toRad(num) {
    return num * Math.PI / 180;
}
function kc(lat1, long1, lat2, long2) {
    var R = 6371;
    if (!!lat2 && !!long2) {
        let φ1 = toRad(lat1);
        let φ2 = toRad(lat2);
        let Δφ = toRad(lat2 - lat1);
        let Δλ = toRad(long2 - long1);
        let a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        let d = R * c;
        return d < 1;
    }
}
exports.getRestaurantNearMe = async (req, res) => {
    const lat1 = req.params.lat;
    const long1 = req.params.long;
    var query = {
        include: [{
            model: db.restaurant,
        }],
    }
    db_Address.findAll(query)
        .then(item => {
            const addressMap = item.filter(addressItem => {
                let lat2 = addressItem.latitude;
                let long2 = addressItem.longitude;
                return kc(lat1, long1, lat2, long2)
            });
            res.status(200).send(addressMap);
        }).catch(err => res.status(400).json({ msg: err }));
};
exports.getAllCommentById = async (req, res) => {
    const check_res = await restaurant.findById(req.params.id);
    if (!check_res) {
        res.status(401).json({ msg: 'Restaurant does not exist' })
    }
    else {
        db_comments.findAll({
            where: {
                idRestaurant: req.params.id
            },
            order: [['createAt', 'DESC']],
        }).then((rs) => {
            res.status(200).send({
                idRestaurant: req.params.id,
                result: rs,
            })
        }).catch(err => res.status(400).json({ msg: err }));
    }
}
exports.getFirst3CommentById = async (req, res) => {
    const check_res = await restaurant.findById(req.params.id);
    if (!check_res) {
        res.status(401).json({ msg: 'Restaurant does not exist' })
    }
    else {
        db_comments.findAll({
            where: {
                idRestaurant: req.params.id
            },
            order: [['createAt', 'DESC']]
        }).then((rs) => {
            res.status(200).send({
                idRestaurant: req.params.id,
                result: rs.slice(0, 3),
            })
        }).catch(err => res.status(400).json({ msg: err }));
    }
}
exports.addComment = async (req, res) => {
    const { idRestaurant, content, name } = req.body;
    const check_res = await restaurant.findById(idRestaurant);
    if (!check_res) {
        res.status(401).json({ msg: 'Restaurant does not exist' })
    }
    else {
        if (typeof(content) === undefined || typeof(name) === undefined)
            res.status(402).json({ msg: 'Params invalid' })
        else {
            db_comments.create({
                idRestaurant: idRestaurant,
                content: content,
                name: name,
                createAt: new Date()
            }).then(rs => {
                res.status(200).json({ msg: 'Have commented successfully', result: rs })
            }).catch(err => res.status(400).json({ msg: err }))
        }
    }
}
