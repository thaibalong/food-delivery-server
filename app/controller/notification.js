const db = require('../config/db.config.js');
const notifications = db.notifications;
const device_token = db.device_token
var passport = require('passport');
const Op = db.Sequelize.Op
var FCM = require('fcm-node');
exports.registerNotification= async (req,res,next) => {
  passport.authenticate('jwt', (err, user) => {
    if (err || !user) {
      return res.status(401).json({
        msg: 'unauthorized',
      });
    }
    let device = {
      idUser: user.id,
      deviceId: req.body.deviceId,
      platform: req.body.platform
    }
    device_token.create(device).then(item=>{
      return res.status(200).send(item);
    }).catch(err=>{
      return res.status(400).json({msg:err});
    })
    
  })(req, res, next);
}
exports.getNotifications = async (req, res, next) => {
  passport.authenticate('jwt', async(err, user) => {
    if (err || !user) {
      return res.status(401).json({
        msg: 'unauthorized',
      });
    }
    let id = user.id
    let noti = await notifications.findAll({
      where: {
        idUser: {
          [Op.or]: [0, id]
        }
      }
    });
    console.log(id)
    return res.status(200).send(noti);
  })(req, res, next);
};
var serverKey = 'AAAAmkn278E:APA91bHOsfFNlLfpKSLGsOgN9prrD-Q8WxqJuTftENAj0FA3rUQk0nhuBkiETLYHsi0W0hUhd2rsOQxvDbx4HrmQGvNJrPIpLriJC6XfloS6Z5QX6dEfhgw_qEst3GCCInB-Ihh66wj5';
var fcm = new FCM(serverKey);
exports.pushNotificationtoUser = (req,res,next)=>{
  passport.authenticate('jwt', async(err, user) => {
    if (err || !user) {
      return res.status(401).json({
        msg: 'unauthorized',
      });
    }
    let data = {
      idUser: user.id,
      title: req.body.title,
      image: req.body.image,
      content: req.body.content
    }
    let device = await device_token.findAll({  attributes: ['deviceId'], where : {idUser: user.id},raw: true})
    device = device.map(i=>i.deviceId)
    var message = { 
      registration_ids: device, 
        notification: {
            title: req.body.title, 
            body: req.body.content 
        },
    };
    fcm.send(message, function(err, response){
        if (err) {
           return res.status(400).json({msg:err});
        } else {
          notifications.create(data).then(result=>{
            return res.status(200).json({msg:"success"})
          }).catch(err=>{
            return res.status(400).json({msg:err});
          })
        }
    });
  })(req,res,next)
}
exports.pushNotificationtoAll = async (req,res,next)=>{
    let data = {
      idUser: 0,
      title: req.body.title,
      image: req.body.image,
      content: req.body.content
    }
    let device = await device_token.findAll({  attributes: ['deviceId'],raw: true})
    device = device.map(i=>i.deviceId)
    var message = { 
      registration_ids: device, 
        notification: {
            title: req.body.title, 
            body: req.body.content 
        },
    };
    fcm.send(message, function(err, response){
        if (err) {
           return res.status(400).json({msg:err});
        } else {
          notifications.create(data).then(result=>{
            return res.status(200).json({msg:"success"})
          }).catch(err=>{
            return res.status(400).json({msg:err});
          })
        }
    });
}