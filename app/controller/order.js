const db = require('../config/db.config.js');
var passport = require('passport');
const order = db.order;
const orderDetail = db.order_detail
const ward = db.ward
const district = db.district
exports.create = async (req, res,next) => {
    let Order, Ward, District
    try {
        passport.authenticate('jwt',async (err, users) => {
            if (err || !users) {
                return res.status(401).json({
                    msg: 'unauthorized',
                });
            }
        Ward = await ward.findById(req.body.idWard)
        District = await district.findById(req.body.idDistrict)
        var ORDER = {
            idUser: users.id,
            totalPrice: req.body.totalPrice,
            date: new Date(),
            address: `${req.body.Street}, ${Ward.name}, ${District.name}`,
            phone: req.body.phone,
            status: 0,
            idRestaurant: req.body.idRestaurant
        }
        Order = await order.create(ORDER)
        var item = req.body.item
        item.map(it => {
            var i = {
                idFood: it.idFood,
                quantity: it.quantity,
                note: it.note,
                idOrder: Order.id,
                idRestaurant: Order.idRestaurant
            }
            orderDetail.create(i)
                .catch(err => {
                    return res.status(400).json({ msg: err })
                })
        })
        return res.status(200).json(Order.id)
    })(req,res,next)
    } catch (err) {
        return res.status(400).json({ msg: err })
    }
}
exports.update = (req, res, next) => {
    id = req.params.id
    order.update(req.body, {
        where: {
            id: id
        }
    }).then(order => {
        res.status(200).json(order)
    }).catch(err => {
        res.status(400).json({ msg: err })
    })
};
exports.getAll = (req, res,next) => {
    passport.authenticate('jwt',async (err, users) => {
        if (err || !users) {
            return res.status(401).json({
                msg: 'unauthorized',
            });
        }
        var query = {
            // include: [{
            // model: db.restaurant
            // }],
            where: { idUser: users.id }, order: [['date', 'DESC']]
        }
    order.findAll(query).then(item=>{
        res.status(200).send(item)
    }).catch(err=>{
        res.status(400).json({msg:err})
    })
})(req,res,next)
};

exports.getOrderById = async (req, res,next) => {
    let id = req.params.id;
    let ord, detail;
    try {
        passport.authenticate('jwt',async (err, users) => {
            if (err || !users) {
                return res.status(401).json({
                    msg: 'unauthorized',
                });
            }
        ord = await order.findOne({where:{id:id,idUser:users.id}})
        detail = await orderDetail.findAll({ where: { idOrder: ord.id} })
        return res.status(200).json({
            order: ord,
            details: detail
        })
        })(req,res,next)
    } catch (err) {
        return res.status(400).json({ msg: err })
    }
};

exports.updateStatus = (req, res) => {
    const status = req.body.status;
    order.update(status, {
        where: {
            id: req.params.idOrder
        }
    }).then(() => {
        res.status(200).json({ msg: 'update status of order successful' });
    }).catch(err => res.status(400).json({ msg: err }))
};

exports.getOrderCompleted = (req, res) => {
    const id = req.params.idUser;
    order.findAll({
        where: {
            status: 'completed',
            idUser: id
        }
    }).then(order => res.status(200).send(order))
        .catch(err => res.status(400).json({ msg: err }))
};

exports.cancelOrder = async (req, res,next) => {
    const id = req.params.id
    passport.authenticate('jwt',async (err, users) => {
        if (err || !users) {
            return res.status(401).json({
                msg: 'unauthorized',
            });
        }
    order.update({ status: -1 }, {
        where: {
            id: id,
            status: 0,
            idUser: users.id
        }
    }).then(item => {
        if (item[0] !== 0)
            res.status(200).json({ msg: "Successfull" })
        else
            res.status(402).json({ msg: "Can't cancel order" })
    }).catch(err => {
        res.status(400).json({ msg: err })
    })
})(req,res,next)
}

exports.createv2 = async (req, res,next) => {
    let Order, Ward, District
    try {
        passport.authenticate('jwt',async (err, users) => {
            if (err || !users) {
                return res.status(401).json({
                    msg: 'unauthorized',
                });
            }
        var ORDER = {
            idUser: users.id,
            totalPrice: req.body.totalPrice,
            date: new Date(),
            address: req.body.address,
            phone: req.body.phone,
            status: 0,
            idRestaurant: req.body.idRestaurant,
            note: req.body.note
        }
        Order = await order.create(ORDER)
        var item = req.body.item
        item.map(it => {
            var i = {
                idFood: it.idFood,
                quantity: it.quantity,
                idOrder: Order.id,
                idRestaurant: Order.idRestaurant
            }
            orderDetail.create(i)
                .catch(err => {
                    return res.status(400).json({ msg: err })
                })
        })
        return res.status(200).json(Order.id)
    })(req,res,next)
    } catch (err) {
        return res.status(400).json({ msg: err })
    }
}