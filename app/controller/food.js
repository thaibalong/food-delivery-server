const db = require('../config/db.config.js');
const food = db.food;

exports.create = (req, res) => {
    food.create(req.body).then(food => {
        res.status(200).json(food);
    }).catch(err => {
        res.status(400).json({ msg: err });
    });
};

exports.update = (req, res, next) => {
    const id = req.params.id;
    food.update(req.body, {
        where: {
            id: id
        }
    }).then(food => {
        res.status(200).json(food)
    }).catch(err => {
        res.status(400).json({ msg: err })
    })
};

exports.getAll = (req, res) => {
    food.findAll().then((food) => { res.status(200).send(food) })
};

exports.getOderById = (req, res) => {
    let id = req.params.id;
    food.findById(id).then((food) => {
        res.status(200).json(food)
    }).catch((err) => {
        res.status(400).json({
            msg: err
        })
    })

};

exports.getFoodById = (req, res) => {
    const idRestaurant = req.params.idRestaurant;
    food.findAll({
        where: {
            idRestaurant: idRestaurant
        }
    }).then(food => {
        res.status(200).send(food);
    }).catch(err => {
        res.status(400).json({ msg: err });
    })
};

exports.getFoodCategory = (req, res) => {
    const idCategory = req.params.idCategory;
    food.findAll({
        where: {
            idFoodCategory: idCategory
        }
    }).then(food => {
        res.status(200).send(food);
    }).catch(err => {
        res.status(400).json({ msg: err });
    })
};

exports.getAllFood = (req, res) => {
    food.findAll()
        .then((food) => res.status(200).send(food))
        .catch(res.status(400))
};

exports.delete = (req, res) => {
    food.destroy({ where: {} }).then(() => { res.send() })
};

exports.searchByName = (req, res) => {
    const name = '%' + req.query.foodname + '%';
    food.sequelize.query("select * from \"FOOD\" where name like '" + name + "' ").then(foods => {
        res.status(200).send(foods[0])
    }).catch(err => {
        res.status(400).json({ msg: err })
    })
};

exports.searchByCategory = (req, res) => {
    const name = '%' + req.query.categoryname + '%';
    query = "select \"FOOD\".* from \"FOOD\", \"FOOD_CATEGORIES\" where \"FOOD_CATEGORIES\".\"name\" like '" + name + "' and \"FOOD\".\"idFoodCategory\" = \"FOOD_CATEGORIES\".\"id\"";
    food.sequelize.query(query).then(foods => {
        res.status(200).send(foods[0])
    }).catch(err => {
        res.status(400).json({ msg: err })
    })
};

exports.getFood = (req,res) => {
    food.findById(req.params.id).then(item=>{
        res.status(200).json({food: item})
    })
}