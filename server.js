var express = require('express');
var bodyParser = require('body-parser');
var passport = require('passport');
const db = require('./app/config/db.config');
var app = express();
db.sequelize.sync();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});
//setting middleware
app.use(express.static(__dirname + '/public')); //Serves resources from public folder
require('./app/config/passport')(passport);
require('./app/router/user')(app, passport);
require('./app/router/restaurant')(app, passport);
require('./app/router/food')(app, passport);
require('./app/router/order')(app, passport);
require('./app/router/address')(app);
require('./app/router/notification')(app, passport)
// require('./app/router/sendNotification')(app, passport)
// require('./app/router/test')(app, passport);

var server_port = process.env.YOUR_PORT || process.env.PORT || 9000;
var server_host = process.env.YOUR_HOST || '0.0.0.0';
app.use('/doc', express.static('doc'));
var server = app.listen(server_port, server_host, function () {
  console.log(`App listening at port ${server_port}`)
});

module.exports = server;